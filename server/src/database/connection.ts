import knex from 'knex'
import knexConfig from '../config/knex'
const environment = process.env.NODE_ENV || 'development'
console.log('environment:', environment)
const db = knex(knexConfig[environment])

export default db
