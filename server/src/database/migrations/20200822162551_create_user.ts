import Knex from 'knex'


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('users', (table) => {
    table.comment('Table to save teachers informations')
    table.increments('id').primary('pk_users').comment('unique ID of users table')
    table.string('name').notNullable().comment('user name')
    table.string('avatar').notNullable().comment('avatar image path')
    table.string('phone').notNullable().comment('cellphone number')
    table.string('bio').notNullable().comment('user biografy')
  })
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('users')
}

