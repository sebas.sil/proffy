import Knex from 'knex'


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('connections', (table) => {
    table.comment('each time that user wants to receive a class clicking on contact button')
    table.increments('id').primary('pk_connections').comment('unique identifier id for connections table')
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now()).comment('When this connection was made (GMT)')

    table.integer('class_id').notNullable().references('id').inTable('classes').index('fk_class_connection').onUpdate('CASCADE').onDelete('RESTRICT')
  })
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('connections')
}

