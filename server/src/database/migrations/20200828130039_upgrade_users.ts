import Knex from 'knex'


export async function up(knex: Knex): Promise<void> {
  await knex.schema.renameTable('users', 'users_old')
  await knex.schema.renameTable('classes', 'classes_old')
  await knex.schema.renameTable('class_schedules', 'class_schedules_old')
  await knex.schema.renameTable('connections', 'connections_old')
  // console.log('UP', 'tables renamed')

  await knex.schema.createTable('users', (table) => {
    table.comment('Table to save users informations')
    table.increments('id').primary('pk_users').comment('unique ID of users table')
    table.string('name').notNullable().comment('user name')
    table.string('avatar').comment('avatar image path')
    table.string('phone').comment('cellphone number')
    table.string('bio').comment('user biografy')
    table.string('email').notNullable().unique().comment('user email and username')
    table.string('password').notNullable().comment('user crypted password')
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now()).comment('The date when this user was created (GMT)')
    table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now()).comment('The date when this user get last updated (GMT)')
  })
  await knex.schema.createTable('classes', (table) => {
    table.comment('Table to save classes informations. A class if given by a teacher')
    table.increments('id').primary('pk_classes').comment('unique identifier id classes table')
    table.string('subject').notNullable().comment('class subject')
    table.decimal('cost').notNullable().comment('teacher class cost')
    table.integer('user_id').notNullable().references('id').inTable('users').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  await knex.schema.createTable('class_schedules', (table) => {
    table.comment('each teacher can save a table of avaliablility (day of week and hours) for each class')
    table.increments('id').primary('pk_classes_schedules').comment('unique identifier id for class schedules table')
    table.integer('week_day').notNullable().comment('Week day of this class (zero is sunday)')
    table.integer('from').notNullable().comment('hour of day that this class starts (minutes from zero)')
    table.integer('to').notNullable().comment('hour of day that this class ends (minutes from zero)')
    table.integer('class_id').notNullable().references('id').inTable('classes').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  await knex.schema.createTable('connections', (table) => {
    table.comment('each time that user wants to receive a class clicking on contact button')
    table.increments('id').primary('pk_connections').comment('unique identifier id for connections table')
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now()).comment('When this connection was made (GMT)')
    table.integer('class_id').notNullable().references('id').inTable('classes').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  // console.log('UP', 'tables created')

  await knex('users').insert(knex('users_old').select('*', knex.raw('id || \'@email.com\' as email'), knex.raw('\'reset_required\' as password'), knex.raw(knex.fn.now() + ' as created_at'), knex.raw(knex.fn.now() + ' as updated_at')))
  await knex('classes').insert(knex('classes_old').select())
  await knex('class_schedules').insert(knex('class_schedules_old').select())
  await knex('connections').insert(knex('connections_old').select())

  // console.log('UP', 'tables copied')

  await knex.schema.dropTable('users_old')
  await knex.schema.dropTable('classes_old')
  await knex.schema.dropTable('class_schedules_old')
  await knex.schema.dropTable('connections_old')
  // console.log('UP', 'tables deleted')

  return new Promise((resolve) => {
    resolve()
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.renameTable('users', 'users_old')
  await knex.schema.renameTable('classes', 'classes_old')
  await knex.schema.renameTable('class_schedules', 'class_schedules_old')
  await knex.schema.renameTable('connections', 'connections_old')
  // console.log('DOWN', 'tables renamed')

  await knex.schema.createTable('users', (table) => {
    table.comment('Table to save teachers informations')
    table.increments('id').primary('pk_users').comment('unique ID of users table')
    table.string('name').notNullable().comment('user name')
    table.string('avatar').notNullable().comment('avatar image path')
    table.string('phone').notNullable().comment('cellphone number')
    table.string('bio').notNullable().comment('user biografy')
  })
  await knex.schema.createTable('classes', (table) => {
    table.comment('Table to save classes informations. A class if given by a teacher')
    table.increments('id').primary('pk_classes').comment('unique identifier id classes table')
    table.string('subject').notNullable().comment('class subject')
    table.decimal('cost').notNullable().comment('teacher class cost')

    table.integer('user_id').notNullable().references('id').inTable('users').index('fk_teacher_class').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  await knex.schema.createTable('class_schedules', (table) => {
    table.comment('each teacher can save a table of avaliablility (day of week and hours) for each class')
    table.increments('id').primary('pk_classes_schedules').comment('unique identifier id for class schedules table')
    table.integer('week_day').notNullable().comment('Week day of this class (zero is sunday)')
    table.integer('from').notNullable().comment('hour of day that this class starts (minutes from zero)')
    table.integer('to').notNullable().comment('hour of day that this class ends (minutes from zero)')

    table.integer('class_id').notNullable().references('id').inTable('classes').index('fk_class_schedule').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  await knex.schema.createTable('connections', (table) => {
    table.comment('each time that user wants to receive a class clicking on contact button')
    table.increments('id').primary('pk_connections').comment('unique identifier id for connections table')
    table.timestamp('created_at').notNullable().defaultTo(knex.fn.now()).comment('When this connection was made (GMT)')

    table.integer('class_id').notNullable().references('id').inTable('classes').index('fk_class_connection').onUpdate('CASCADE').onDelete('RESTRICT')
  })
  // console.log('DOWN', 'tables created')

  await knex(
    'users'
  ).insert(
    knex.select([
      'id', 
      'name', 
      knex.raw(`(case when avatar is null then 'https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png' else avatar end) as avatar`), 
      knex.raw(`(case when phone is null then '-' else phone end) as phone`), 
      knex.raw(`(case when bio is null then '-' else bio end) as bio`)
    ]).from('users_old'))
  
  await knex('classes').insert(knex('classes_old').select())
  await knex('class_schedules').insert(knex('class_schedules_old').select())
  await knex('connections').insert(knex('connections_old').select())
  // console.log('DOWN', 'tables copied')


  await knex.schema.dropTable('users_old')
  await knex.schema.dropTable('classes_old')
  await knex.schema.dropTable('class_schedules_old')
  await knex.schema.dropTable('connections_old')
  // console.log('DOWN', 'tables deleted')

  return new Promise((resolve) => {
    resolve()
  })
}

