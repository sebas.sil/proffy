import Knex from 'knex'


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('class_schedules', (table) => {
    table.comment('each teacher can save a table of avaliablility (day of week and hours) for each class')
    table.increments('id').primary('pk_classes_schedules').comment('unique identifier id for class schedules table')
    table.integer('week_day').notNullable().comment('Week day of this class (zero is sunday)')
    table.integer('from').notNullable().comment('hour of day that this class starts (minutes from zero)')
    table.integer('to').notNullable().comment('hour of day that this class ends (minutes from zero)')

    table.integer('class_id').notNullable().references('id').inTable('classes').index('fk_class_schedule').onUpdate('CASCADE').onDelete('RESTRICT')
  })
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('class_schedules')
}

