import Knex from 'knex'


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('classes', (table) => {
    table.comment('Table to save classes informations. A class if given by a teacher')
    table.increments('id').primary('pk_classes').comment('unique identifier id classes table')
    table.string('subject').notNullable().comment('class subject')
    table.decimal('cost').notNullable().comment('teacher class cost')

    table.integer('user_id').notNullable().references('id').inTable('users').index('fk_teacher_class').onUpdate('CASCADE').onDelete('RESTRICT')
  })
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('classes')
}

