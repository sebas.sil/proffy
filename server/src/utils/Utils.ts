/**
 * convert hours (string) to minutes or seconds
 * @param {string} val hours in string (ex: 08:10 or 10:30:14)
 * @return {number} if the time has only hours, it returns just the hour,
 *                  if it has minutes it returns the time in minutes and
 *                  if it has seconds, it returns the time in seconds...
 */
const hoursToMinutes = (val: string): number => val.split(':').map(Number).reduce((acc, time) => (60 * acc) + time)

/**
 * convert minutes (number) to hours (string)
 * @param {number} val minuts in number format (ex: 460)
 * @return {string} returns hours and minutes formated like hh:mm
 */
const minutesToHours = (val: number): string => {
  const hours = Math.floor(val / 60)
  const minutes = Math.floor(val % 60)

  const conv = (num:number):string => num.toString().padStart(2, '0')
  const timeString = conv(hours) + ':' + conv(minutes)

  return timeString
}


export {hoursToMinutes, minutesToHours}