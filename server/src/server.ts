import dotenv from 'dotenv'
dotenv.config({path: __dirname + '/config/.env'})

import express from 'express'
import AuthRoutes from './Routes/Auth.routes'
import AppRoutes from './Routes/routes'
import cors from 'cors'

/**
 * The start point of this aplication
 */
const app = express()

// midware to enable CORs just to localhost
/* app.use((req, res, next) => {
    console.log('entrou')
    res.header('Access-Control-Allow-Origin', 'http://localhost:3333')
    res.header('Access-Control-Allow-Methods', ['GET'])
    next()
})*/
app.use(cors())
const server = app.listen(3333)
app.use(express.json())
app.use('/auth', AuthRoutes)
app.use(AppRoutes)

export default server
