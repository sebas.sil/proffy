import db from '../database/connection'
import {IClass, IUser, ISchedule} from './AuthController'
import {RequiredFieldError} from '../exceptions/CustomErrors'

const TAG = 'DB'

/**
 * Encharged to do database operations for classes object
 */
export default class ClassesController {

  /**
   * Create a class, teacher and schedule items of a class
   * @param {number} id user id, class owner
   * @param {IClass} clazz object
   * @return {IClass} inserted class
   */
  async create(id: number, clazz: IClass): Promise<IClass> {
    console.log(TAG, 'create class to user', id)
    const trx = await db.transaction()

    try {
      const insertedClassesId = await trx('classes').insert({
        subject: clazz.subject,
        cost: clazz.cost,
        user_id: id,
      })

      if (clazz.schedule && clazz.schedule.length > 0) {
        clazz.schedule.forEach((item) => {
          item.class_id = insertedClassesId[0]
          delete (item as any).id
        })
        

        await trx('class_schedules').insert(clazz.schedule)
      }
      await trx.commit()
      return this.index(insertedClassesId[0])
    } catch (e) {
      if (e.errno === 19 && String(e.message).indexOf('SQLITE_CONSTRAINT: NOT NULL') > 0) {
        const arr = String(e.message).match(/.+: .+\.(.+)/)
        e = new RequiredFieldError(arr && arr.length > 1 ? arr[1] : 'one field')
      }
      await trx.rollback()
      throw (e)
    }
  }

  /**
   * Create a class, teacher and schedule items of a class
   * @param {number} id user id, class owner
   * @param {IClass} clazz object
   * @return {IClass} inserted class
   */
  async update(id: number, clazz: IClass): Promise<IClass> {
    console.log(TAG, 'updating class to user', id)
    const trx = await db.transaction()

    try {
      await trx('classes').update({
        subject: clazz.subject,
        cost: clazz.cost
      }).where({id: clazz.id})

      clazz.schedule.forEach(s => {
        s.class_id = clazz.id
        delete (s as any).id
      })

      await trx('class_schedules').delete().where('class_id', '=', clazz.id)
      await trx('class_schedules').insert(clazz.schedule)

      await trx.commit()
      return this.index(clazz.id)
    } catch (e) {
      if (e.errno === 19 && String(e.message).indexOf('SQLITE_CONSTRAINT: NOT NULL') > 0) {
        const arr = String(e.message).match(/.+: .+\.(.+)/)
        e = new RequiredFieldError(arr && arr.length > 1 ? arr[1] : 'one field')
      }
      await trx.rollback()
      throw (e)
    }
  }


  async index(subject: string, hour?: number, week_day?: string): Promise<IUser[]>
  async index(id: number): Promise<IClass>

  /**
     * Search classes by filter (requireds)
     * @param {string} subject class subject
     * @param {number} hour desired class hour @see {@link routes.ts#hoursToMinutes}
     * @param {number} week_day week number day (sunday = 0)
     */
  async index(...args: any): Promise<any> {
    if (args.length === 1) {
      return this.getClassById(args[0])
    } else {
      return this.getUserByClassSchedule(args[0], args[1], args[2])
    }
  }

  private async getUserByClassSchedule(subject: string, hour: number, week_day: string): Promise<IUser[]> {
    const sql = db('classes')
      .join('users', 'user_id', '=', 'users.id')
      .join('class_schedules', 'class_id', '=', 'classes.id')
      .where('subject', '=', subject)
      if(hour) {
        sql.whereBetween(hour, [db.raw('`from`'), db.raw('`to`')])
      }
      if(week_day) {
        sql.where('week_day', '=', week_day)
      }
      return sql.then((rows: Array<any>) => {
        const us = [] as Array<IUser>
        rows.forEach((row) => {
          let u = us.find((i) => i.id === row.user_id)
          if (!u) {
            u = {
              id: row.user_id,
              name: row.name,
              avatar: row.avatar,
              phone: row.phone,
              bio: row.bio,
              email: row.email,
              class: {
                id: row.class_id,
                subject: row.subject,
                cost: row.cost,
                schedule: [],
              },
              password: '',
              created_at: row.created_at,
              updated_at: row.updated_at
            }
            us.push(u)
          }

          u.class.schedule.push({
            id: row.class_schedules_id,
            week_day: row.week_day,
            from: row.from,
            to: row.to,
          } as ISchedule)
        })

        return us
      })
  }

  private async getClassById(id: number): Promise<IClass> {
    return db('classes')
      .join('class_schedules', 'class_id', '=', 'classes.id')
      .where('classes.id', '=', id).then((rows: Array<any>) => {
        let c = {} as IClass
        rows.forEach((row) => {
          console.log(row)
          if (!c.id) {
            c = {
              id: row.class_id,
              subject: row.subject,
              cost: row.cost,
              schedule: []
            }
          }

          c.schedule.push({
            id: row.id,
            week_day: row.week_day,
            from: row.from,
            to: row.to,
          } as ISchedule)
        })

        return c
      })
  }
}
