import db from '../database/connection'

/**
 * Encharged to do database operations for users object
 */
export default class ConnectionController {
  /**
   * Search the total os connections between students and teachers
   */
  async index() {
    const total = await db('connections').count('*', {as: 'total'}).first()
    return total
  }

  /**
   * Create a connection between an student and a teacher
   * @param {number} id, class id
   */
  async create(id: string) {
    await db('connections').insert({class_id: Number(id)})
  }
}
