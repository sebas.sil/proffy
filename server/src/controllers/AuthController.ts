import db from '../database/connection'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import mailer, {Options} from '../config/mailer'
import {RequiredFieldError, AlreadExistsError} from '../exceptions/CustomErrors'

const TAG = 'DB'
/**
 * Express an availability of a class
 */
interface IUser {
  id: number
  name: string
  avatar: string
  phone: string
  bio: string
  email: string
  password: string
  created_at: Date
  updated_at: Date
  class: IClass
}

interface IAuthResponse {
  user: IUser,
  token: string
}

interface IClass {
  id: number
  subject: string
  cost: number
  schedule: Array<ISchedule>
}

interface ISchedule {
  id: number
  week_day: number
  from: number | string
  to: number | string
  class_id: any
}

/**
 * Encharged to do database operations for authentications
 * and user auth operations
 */
class AuthController {
  /**
   * Generate a JWT token to identify an user
   * @param {IUser} user to be inserted inside JWT token
   * @param {number} expiresIn max live time of this token
   * @return {string} JWT token with user information
   */
  private generateJWT(user: IUser, expiresIn: number = 600): string {
    console.log(TAG, 'generate JWT', user.email)
    if (!process.env.SECRET) {
      throw Error('secret isnt defined')
    }
    const token = jwt.sign(user, process.env.SECRET, {
      expiresIn,
    })
    return token
  }

  /**
     * Create an user
     * @param {IUser} user user to be saved
     * @return {Promise<IAuthResponse>} object with an user and an JWT token
     * @throws {UserAlreadExistsError} User already exists
     * @throws {RequiredFieldError} required field missing
     */
  async create(user: IUser): Promise<IAuthResponse> {
    console.log(TAG, 'crete user', user.email)

    if (user.id) throw new Error('User cant be created with id')
    if (!user.password) throw new RequiredFieldError('password')
    if (!user.email) throw new RequiredFieldError('email')
    // delete user.updated_at
    // delete user.created_at

    const hash = bcrypt.hashSync(user.password, 10)
    user.password = hash

    user.email = user.email.toLowerCase()

    return db('users').insert(user)
        .then(() =>
          db<IUser>('users').select().where('email', '=', user.email)
              .first().then((found) => {
                if (found) {
                  found.password = ''
                  const token = this.generateJWT(found)
                  return {user: found, token}
                } else {
                  throw new Error('None user found after inserting')
                }
              }),
        ).catch((e) => {
          if (e.errno === 19 && String(e.message).indexOf('SQLITE_CONSTRAINT: UNIQUE') > 0) {
            throw new AlreadExistsError(user.email)
          } else if (e.errno === 19 && String(e.message).indexOf('SQLITE_CONSTRAINT: NOT NULL') > 0) {
            const arr = String(e.message).match(/.+: .+\.(.+)/)
            throw new RequiredFieldError(arr && arr.length > 1 ? arr[1] : 'one field')
          } else {
            throw new Error(e)
          }
        })
  }

  /**
     * Update an user
     * @param {IUser} user user to be modified
     */
  async update(user: IUser): Promise<IUser> {
    console.log(TAG, 'update user', user.id)
    return db('users').where('id', '=', user.id as number).update(
        {
          email: user.email.toLowerCase(),
          avatar: user.avatar,
          bio: user.bio,
          name: user.name,
          phone: user.phone,
          updated_at: db.fn.now(),
        },
    ).then(async () => {
      return db<IUser>('users').select().where('email', '=', user.email.toLowerCase())
          .first().then((found) => {
            if (found) {
              found.password = ''
              return found
            } else {
              throw new Error('')
            }
          })
    })
  }


  /**
   * Authenticate an user
   * @param {string} email user username
   * @param {string} password user password
   * @return {IAuthResponse} authentication token and user
   */
  async authenticate(email: string, password: string): Promise<IAuthResponse> {
    console.log(TAG, 'authenticate user', email, password)

    if (!email) throw new RequiredFieldError('email')
    if (!password) throw new RequiredFieldError('password')

    const found = await db<IUser>('users')
        .select().where('email', '=', email.toLowerCase()).first()
    let ret = undefined
    if (found) {
      const ok = await bcrypt.compare(password, found.password)
      if (ok) {
        const clazz = await db<IClass>('classes')
            .select().where('user_id', '=', found.id as number).first()
        if (clazz) {
          found.class = clazz
          const schedule = await db('class_schedules')
              .select(['id', 'week_day', 'from', 'to'])
              .where('class_id', '=', clazz.id)
          clazz.schedule = schedule
        }

        found.password = ''

        const token = this.generateJWT(found)
        ret = {user: found, token}
      } else {
        throw Error('Wrong username or password')
      }
    } else {
      throw Error('Wrong username or password')
    }
    return ret
  }

  /**
   * Send and e-mail to user with reset password URL
   * @param {string} email user e-mail (username)
   */
  async forgotPassword(email: string): Promise<void> {
    console.log(TAG, 'forgot password', email)

    if (!email) throw new RequiredFieldError('email')

    const found = await db<IUser>('users')
        .select().where('email', '=', email.toLowerCase()).first()
    if (found) {
      const token = this.generateJWT({email: found.email} as IUser)
      mailer.sendMail({
        to: found.email,
        from: process.env.SMTP_FROM,
        subject: 'Proffy password reset',
        template: 'forgot_password',
        context: {
          token, email,
        },
      } as Options, (err) => {
        if (err) {
          throw err
        }
      })
    }
  }

  /**
   * Method to reset user password
   * @param {string} email user email (username) to have the password reseted
   * @param {string} password new user password
   * @param {string} token authentication token
   * @return {IAuthResponse} authentication token and user
   */
  async resetPassword(email: string, password: string, token: string): Promise<IAuthResponse> {
    console.log(TAG, 'reset password with token', email)

    if (!password) throw new RequiredFieldError('password')
    if (!email) throw new RequiredFieldError('email')
    if (!token) throw new RequiredFieldError('token')
    if (!process.env.SECRET) {
      throw Error('secret isnt defined')
    }

    const decode = jwt.verify(token, process.env.SECRET) as IUser

    if (decode.email.toLowerCase() !== email.toLowerCase()) {
      throw Error('Action not permited')
    }
    return db<IUser>('users').select()
        .where('email', '=', decode.email.toLowerCase())
        .first().then(async (found) => {
          if (found) {
            const hash = bcrypt.hashSync(password, 10)
            return db<IUser>('users').where({email}).update({'password': hash, 'updated_at': db.fn.now()}).then(() => {
              found.password = ''
              return {user: found, token}
            })
          } else {
            throw Error('Action not permited')
          }
        })
  }
}


export type {IUser, IClass, ISchedule}
export default AuthController
