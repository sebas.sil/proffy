/**
 * Super class of all validation errors into this system
 */
class ValidationError extends Error {
  /**
   * default constructor
   * @param {string} message string message displayed to this error
   */
  constructor(message: string) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

/**
 * Erro related to missings fields (required info not provided)
 */
class RequiredFieldError extends ValidationError {
  /**
   * default constructor
   * @param {string} message string message displayed to this error
   */
  constructor(field: string) {
    super(field + ' is required')
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

/**
 * When creating an user with same unique field the alread exists
 */
class AlreadExistsError extends ValidationError {
  /**
   * default constructor
   * @param {string} message string message displayed to this error
   */
  constructor(value: string) {
    super(value + ' alread exists')
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

export {ValidationError, RequiredFieldError, AlreadExistsError}
