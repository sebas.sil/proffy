import Express from 'express'
import ClassesController from '../controllers/ClassesController'
import ConnectionController from '../controllers/ConnectionController'
import authMiddleware, {IGetUserAuthInfoRequest} from '../middlewares/auth'
import UserController, {IUser, IClass} from '../controllers/AuthController'
import {ValidationError, RequiredFieldError} from '../exceptions/CustomErrors'
import {hoursToMinutes, minutesToHours} from '../utils/Utils'

const routes = Express.Router()

const controllClass = new ClassesController()
const controllConns = new ConnectionController()
const controllUser = new UserController()

const TAG = 'ROUTE_APP'

routes.use((req, res, net) => authMiddleware(req as IGetUserAuthInfoRequest, res, net))

/**
 * Create a route to 'users' resource
 * It's responsible to create a user
 */
routes.post('/update', async (req, res) => {
  console.log(TAG, 'POST', '/update')
  const authUser = (req as IGetUserAuthInfoRequest).user
  const u = req.body as IUser

  // verify if the authenticated user is modifying it wown attributes
  if (u.id === authUser.id) {
    try {
      const user = await controllUser.update(u)
      res.send(user)
    } catch (e) {
      console.log(e)
      res.status(500).json({message: 'Error updating user'})
    }
  } else {
    res.status(400).json({message: 'Not permited'})
  }
  return res
})

/**
 * Create a route to 'classes' resource
 * It's responsible to create a class, teacher and schedule
 */
routes.post('/classes', async (req, res) => {
  console.log('POST', '/classes')
  const clazz = req.body as IClass
  const user = (req as IGetUserAuthInfoRequest).user.id
  try {
    if (!clazz.schedule) throw new RequiredFieldError('schedule')
    clazz.schedule.forEach((item:any) => {
      item.from = hoursToMinutes(item.from)
      item.to = hoursToMinutes(item.to)
    })

    let c
    if(clazz.id > 0){
      c = await controllClass.update(user, clazz)
      res.status(200)
    } else {
      c = await controllClass.create(user, clazz)
      res.status(201)
    }
    if(c?.schedule){
      c.schedule.forEach(s => {
        s.from = minutesToHours(s.from as number)
        s.to = minutesToHours(s.to as number)
      })
    }
    res.send(c)
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).json(e.message)
    } else {
      console.log(e)
      res.status(500).json({message: 'Error creating class'})
    }
  }
  return res
})

/**
 * Create a route to 'classes' resource
 * It's responsible to get classes by filter
 * ** filters are required
 */
routes.get('/classes', async (req, res) => {
  console.log('GET', '/classes', req.query)
  try {
    const week_day = req.query.week_day as string
    const subject = req.query.subject as string
    const time = req.query.time as string

    if (subject) {
      const hours = time ? hoursToMinutes(time) : undefined
      const teachers = await controllClass.index(subject, hours, week_day)

      teachers.forEach(t => {
        t.class.schedule.forEach((s:any) => {
          s.from = minutesToHours(s.from)
          s.to = minutesToHours(s.to)
        })
      })
      res.json(teachers)
    } else {
      res.status(400).json({message: 'Missing filters search parameters'})
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({message: 'Error searching class'})
  }
  return res
})

/**
 * Create a route to 'connections' resource
 * It's responsible to insert a new connection into connections table
 */
routes.post('/connections', async (req, res) => {
  console.log('POST', '/connections', req.body)
  try {
    const {class_id} = req.body

    if (class_id) {
      await controllConns.create(class_id as string)
      res.status(201).send()
    } else {
      res.status(400).json({message: 'Missing required field id'})
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({message: 'Error creating connection'})
  }
  return res
})

/**
 * Create a route to 'connections' resource
 * It's responsible to get the total of connections registered
 */
routes.get('/connections', async (req, res) => {
  console.log('GET', '/connections')
  try {
    const total = await controllConns.index()
    res.json(total)
  } catch (e) {
    console.log(e)
    res.status(500).json({message: 'Error creating connection'})
  }
  return res
})

routes.all('*', async (req, res) => {
  res.status(404).send()
})

export default routes
