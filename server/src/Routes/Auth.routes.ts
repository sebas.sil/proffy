import express from 'express'
import UserController, {IUser} from '../controllers/AuthController'
import {ValidationError} from '../exceptions/CustomErrors'
import {minutesToHours} from '../utils/Utils'
/**
 * Create unauthenticated routes.
 * This can be accessed by unauthenticated users to authenticate or register
 */

const TAG = 'ROUTE_AUTH'
const routes = express.Router()
const controllUser = new UserController()

/**
 * Create a route to 'users' resource
 * It's responsible to create a user
 */
routes.post('/register', async (req, res) => {
  console.log(TAG, 'POST', '/register')
  const {name, avatar, phone, bio, email, password} = req.body
  // console.log(name, avatar, phone, bio, email, password)
  try {
    const user = await controllUser.create({name, avatar, phone, bio, email, password} as IUser)
    res.status(201).send(user)
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).json(e.message)
    } else {
      console.error(e)
      res.status(500).json({message: 'Error registering user'})
    }
  } finally {
    return res
  }
})

/**
 * Create a route to 'users' resource
 * It's responsible to create a user
 */
routes.post('/authenticate', async (req, res) => {
  console.log(TAG, 'POST', '/authenticate')
  const {email, password} = req.body
  console.log(email, password)
  try {
    const auth = await controllUser.authenticate(email, password)
    if (auth) {
      if (auth.user.class && auth.user.class.schedule) {
        auth.user.class.schedule.forEach(s => {
          s.from = minutesToHours(s.from as number),
          s.to = minutesToHours(s.to as number)
        })
      }
      res.send(auth)
    } else {
      res.status(401).json({message: 'Bad username or password'})
    }
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).json(e.message)
    } else {
      console.error(e)
      res.status(500).json({message: 'Error validating user credentials'})
    }
  }
  return res
})

/**
 * Create a route to 'forgot_password' resource
 * Send mail to user to reset password
 */
routes.post('/forgot_password', async (req, res) => {
  console.log(TAG, 'POST', 'forgot_password', req.body)
  const {email} = req.body

  try {
    await controllUser.forgotPassword(email)
    res.json({message: 'Reset password e-mail sent (if this e-mail exists)'})
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).json(e.message)
    } else {
      console.error(e)
      res.status(500).json({message: 'Error sending reset password e-mail'})
    }
  }

  return res
})

/**
 * Create a route to 'reset_password' resource
 * Verify reset password token and change the password
 */
routes.post('/reset_password', async (req, res) => {
  console.log(TAG, 'POST', 'reset_password')
  const {email, password, token} = req.body

  try {
    const auth = await controllUser.resetPassword(email, password, token)
    if (auth.user.class && auth.user.class.schedule) {
      auth.user.class.schedule.forEach(s => {
        s.from = minutesToHours(s.from as number),
        s.to = minutesToHours(s.to as number)
      })
    }
    res.status(200).send(auth)
  } catch (e) {
    if (e instanceof ValidationError) {
      res.status(400).json(e.message)
    } else {
      console.error(e)
      res.status(500).json({message: 'Error reseting password with token'})
    }
  }

  return res
})

routes.all('*', async (req, res) => {
  res.status(404).send()
})

export default routes
