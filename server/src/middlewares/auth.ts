import {Response, Request, NextFunction} from 'express'
import jwt from 'jsonwebtoken'
import {IUser} from '../controllers/AuthController'

interface IGetUserAuthInfoRequest extends Request {
  user: IUser
}

const authMiddleware = (req: IGetUserAuthInfoRequest, res: Response, next: NextFunction) => {
  if (!process.env.SECRET) {
    throw Error('secret isnt defined')
  }
  let ok = false
  try {
    const headers = req.headers.authorization
    if (headers) {
      const parts = headers.split(' ')
      if (parts.length === 2) {
        const [schema, token] = parts

        if (/^Bearer$/i.test(schema)) {
          jwt.verify(token, process.env.SECRET, (err, decoded) => {
            if (!err) {
              ok = true
              req.user = decoded as IUser
            }
          })
        }
      }
    }
  } catch (e) {
    console.error('Error validating token', e)
  }
  if (!ok) return res.status(401).send({error: 'invalid token'})
  else return next()
}

export {IGetUserAuthInfoRequest}
export default authMiddleware
