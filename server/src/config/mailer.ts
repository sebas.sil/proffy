import nodemailer, {TransportOptions} from 'nodemailer'
import hbs from 'nodemailer-express-handlebars'
import path from 'path'

const transport = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_LOGIN,
    pass: process.env.SMTP_PASSWORD,
  },

} as TransportOptions)

transport.use('compile', hbs({
  viewEngine: {
    extname: '.html',
    layoutsDir: path.resolve(__dirname, 'views'),
    partialsDir: path.resolve(__dirname, 'views'),
    defaultLayout: '',
  },
  extName: '.html',
  viewPath: path.resolve(__dirname, 'views'),
}))

export {Options} from 'nodemailer/lib/mailer'
export default transport
