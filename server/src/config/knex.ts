import path from 'path'

interface KnexConfig {
  [key: string]: object;
};

/**
 * database configuration over knex with database
 * If the database change to another (from sqlite3 to MySQL or Oracle),
 * this configuration must change
 */
const knexConfig:KnexConfig = {
  test: {
    client: 'sqlite3',
    connection: ':memory:',
    migrations: {
      directory: path.resolve(__dirname, '..', 'database', 'migrations'),
    },
    useNullAsDefault: true,
  },
  development: {
    client: 'sqlite3',
    connection: {
      filename: path.resolve(__dirname, '..', 'database', 'proffy.db3'),
    },
    migrations: {
      directory: path.resolve(__dirname, '..', 'database', 'migrations'),
    },
    useNullAsDefault: true,
  },
}


export default knexConfig
