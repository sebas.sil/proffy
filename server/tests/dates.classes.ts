const dts = {
    all_ok: [
        {
            "subject": "matematica",
            "cost": 101.99,
            "schedule": [
                {
                    "week_day": 0,
                    "from": "09:00",
                    "to": "10:00"
                },
                {
                    "week_day": 1,
                    "from": "18:00",
                    "to": "22:00"
                }
            ]
        }
    ],
    mix: [
        {
            "cost": 101.99,
            "schedule": [
                {
                    "week_day": 0,
                    "from": "09:00",
                    "to": "10:00"
                },
                {
                    "week_day": 0,
                    "from": "09:00",
                    "to": "10:00"
                }
            ]
        },
        {
            "subject": "matematica",
            "schedule": [
                {
                    "week_day": 0,
                    "from": "09:00",
                    "to": "10:00"
                },
                {
                    "week_day": 0,
                    "from": "09:00",
                    "to": "10:00"
                }
            ]
        },
        {
            "subject": "matematica",
            "cost": 101.99
        },
        {
            "subject": "matematica",
            "cost": 101.99,
            "schedule": [ ]
        }
    ]
}

export default dts