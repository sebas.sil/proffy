import users_cases from './dates'
import classes_cases from './dates.classes'
import db from '../src/database/connection'

import supertest from 'supertest'
import server from '../src/server'
import {IUser} from '../src/controllers/AuthController'

const choice = Math.floor(Math.random() * users_cases.all_ok.length - 1)
const all_ok = classes_cases.all_ok
const no_subject = classes_cases.mix.filter(c => !c.subject && c.cost && c.schedule)
const no_cost = classes_cases.mix.filter(c => !c.cost && c.subject && c.schedule)
const no_schedules = classes_cases.mix.filter(c => !c.schedule && c.subject && c.cost)

let token: string = ''
/**
 * close database conection
 */
afterAll(async (done) => {
    try {
        // console.log('deleting tables')
        do {
            // testing rollback
            await db.migrate.rollback()
        } while (await db.migrate.currentVersion() !== 'none')
        // console.log('disconecting')
        await db.destroy()
        server.close()
    } finally {
        done()
    }
})

/**
 * create database
 */
beforeAll(async (done) => {
    // console.log('preparing database')
    // testing migrations
    try {
        await db.migrate.latest()
        const user = users_cases.all_ok[choice]
        await supertest(server).post('/auth/register').send(user).then(async response => {
            console.log('logged in as ', response.body.user.id, response.body.user.name)
            token = response.body.token
        })
        //TODO make seed with classes and schedules to test get
    } finally {
        done()
    }
})

describe('POST /classes', () => {

    test.each(all_ok)('creating class OK %# %s', (clazz) => {
        return supertest(server).post('/classes').set('Authorization', 'Bearer ' + token).send(clazz).then(response => {
            expect(response.status).toBe(201)
            expect(response.body).toMatchObject({})
        })
    })

    test.each(no_subject)('creating class no sub %# %s', (clazz) => {

        return supertest(server).post('/classes').set('Authorization', 'Bearer ' + token).send(clazz).then(response => {
            expect(response.status).toBe(400)
            expect(response.body).toBe('subject is required')
        })
    })

    test.each(no_cost)('creating class no cost %# %s', async (clazz) => {
        return supertest(server).post('/classes').set('Authorization', 'Bearer ' + token).send(clazz).then(response => {
            expect(response.status).toBe(400)
            expect(response.body).toBe('cost is required')
        })
    })

    test.each(no_schedules)('creating class no cost %# %s', async (clazz) => {
        return supertest(server).post('/classes').set('Authorization', 'Bearer ' + token).send(clazz).then(response => {
            expect(response.status).toBe(400)
            expect(response.body).toBe('schedule is required')
        })
    })
})


describe('GET /classes', () => {

    test.each(all_ok)('getting class OK %# %s', async (clazz) => {
        return supertest(server).get('/classes').set('Authorization', 'Bearer ' + token).query({subject: clazz.subject, time: clazz.schedule[1].from, week_day: clazz.schedule[1].week_day}).then(response => {
            expect(response.status).toBe(200)
            expect(response.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        class: expect.objectContaining({
                            subject: clazz.subject,
                            cost: clazz.cost,
                            schedule: expect.arrayContaining([clazz.schedule[1]])
                        })
                    })
                ])
            )
        })
    })
})