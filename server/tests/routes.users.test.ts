import cases from './dates'
import supertest from 'supertest'
import server from '../src/server'
import db from '../src/database/connection'

import nodemailer from '../src/config/mailer'
import {IUser} from '../src/controllers/AuthController'
jest.mock('../src/config/mailer.ts');
const mockedNodemailer = nodemailer as jest.Mocked<typeof nodemailer>;

// random get x numbers (to choice the users that will be tested)
const idxs: Array<number> = Array.from({length: 4}, () => Math.floor(Math.random() * cases.all_ok.length - 1))
// qtd of registers to be tested in each type (qtd/1000 chance to get an invalid index)
const qtd = 1
//map is just to print email on the test name
console.log(idxs)
const ok_to_register = cases.all_ok.splice(idxs.pop() || 0, qtd).map(u => Object.assign(u, {toString: () => u.email}))
const no_password = cases.all_ok.splice(idxs.pop() || 0, qtd).map(u => Object.assign(u, {toString: () => (u.email + '(' + u.password + ')')})).map(u => {const u1 = u as any; delete u1.password; return u1 as IUser})
const no_email = cases.all_ok.splice(idxs.pop() || 0, qtd).map(u => Object.assign(u, {toString: () => (u.email + '(' + u.name + ')')})).map(u => {const u1 = u as any; delete u1.email; return u1 as IUser})
const no_name = cases.all_ok.splice(idxs.pop() || 0, qtd).map(u => Object.assign(u, {toString: () => (u.email + '(' + u.name + ')')})).map(u => {const u1 = u as any; delete u1.name; return u1 as IUser})

/**
 * close database conection
 */
afterAll(async (done) => {
    try {
        console.log('deleting tables')
        //do {
        // testing rollback
        //    await db.migrate.rollback()
        //} while (await db.migrate.currentVersion() !== 'none')
        //console.log('disconecting')
        await db.destroy()
        server.close()
    } finally {
        done()
    }
})

/**
 * create database
 */
beforeAll(async (done) => {
    console.log('preparing database')
    // testing migrations
    try {
        await db.migrate.latest()
    } finally {
        done()
    }
})


describe('POST /auth/register', () => {
    test.each(ok_to_register)('registering OK %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user).then(response => {
            expect(response.status).toBe(201)
            expect(response.body).toHaveProperty('user')
            expect(response.body).toHaveProperty('token')
        })
    })

    test.each(no_password)('registering no pass %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user).then(response => {
            expect(response.badRequest).toBeTruthy()
            expect(response.status).toBe(400)
            expect(response.body).not.toHaveProperty('user')
            expect(response.body).not.toHaveProperty('token')
            expect(response.body).toBe('password is required')
        })
    })

    test.each(no_email)('registering no_email %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user).then(response => {
            expect(response.badRequest).toBeTruthy()
            expect(response.status).toBe(400)
            expect(response.body).not.toHaveProperty('user')
            expect(response.body).not.toHaveProperty('token')
            expect(response.body).toBe('email is required')
        })
    })

    test.each(no_name)('registering no name %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user).then(response => {
            expect(response.badRequest).toBeTruthy()
            expect(response.status).toBe(400)
            expect(response.body).not.toHaveProperty('user')
            expect(response.body).not.toHaveProperty('token')
            expect(response.body).toBe('name is required')
        })
    })
})


describe('POST /auth/authenticate', () => {
    test.each(ok_to_register)('authenticating OK %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user).then(async () => {
            return supertest(server).post('/auth/authenticate').send({email: user.email, password: user.password})
                .then(response => {
                    expect(response.badRequest).not.toBeTruthy()
                    expect(response.status).toBe(200)
                    expect(response.body).toHaveProperty('user')
                    expect(response.body).toHaveProperty('token')
                })
        })
    })

    test.each(no_password)('authenticating no pass %# %s', async (user) => {
        return supertest(server).post('/auth/register').send({...user, password: 'Welcome1'}).then(async () => {
            return supertest(server).post('/auth/authenticate').send({email: user.email, password: null})
                .then(response => {
                    expect(response.badRequest).toBeTruthy()
                    expect(response.status).toBe(400)
                    expect(response.body).toBe('password is required')
                })
        })
    })

    test.each(no_email)('authenticating no email %# %s', async (user) => {
        return supertest(server).post('/auth/register').send({...user, email: user.password + '@fake.com'}).then(async () => {
            return supertest(server).post('/auth/authenticate').send({email: user.email, password: user.password})
                .then(response => {
                    expect(response.badRequest).toBeTruthy()
                    expect(response.status).toBe(400)
                    expect(response.body).toBe('email is required')
                })
        })
    })

    test.each(no_name)('authenticating no name %# %s', async (user) => {
        return supertest(server).post('/auth/register').send({...user, name: 'fake name'}).then(async () => {
            return supertest(server).post('/auth/authenticate').send({email: user.email, password: user.password})
                .then(response => {
                    expect(response.badRequest).not.toBeTruthy()
                    expect(response.status).toBe(200)
                    expect(response.body).toHaveProperty('user')
                    expect(response.body).toHaveProperty('token')
                })
        })
    })
})

describe('POST /auth/forgot_password', () => {

    test.each(ok_to_register)('forgot_password OK %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        expect(response.status).toBe(200)
                        expect(token).not.toBeNull()
                    })
            })
    })

    test.each(no_email)('forgot_password no email %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: null})
                    .then(response => {
                        expect(response.badRequest).toBeTruthy()
                        expect(response.status).toBe(400)
                        expect(response.body).toBe('email is required')
                    })
            })
    })

    test.each(no_password)('forgot_password no pass %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        expect(response.status).toBe(200)
                        expect(token).not.toBeNull()
                    })
            })
    })

    test.each(no_name)('forgot_password no name %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        expect(response.status).toBe(200)
                        expect(token).not.toBeNull()
                    })
            })
    })
})

describe('POST /auth/reset_password', () => {

    test.each(ok_to_register)('reset_password OK %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(async response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        return supertest(server).post('/auth/reset_password').send({email: user.email, password: user.password, token})
                            .then(response => {
                                if (!response.badRequest) {
                                    expect(response.status).toBe(200)
                                } else {
                                    throw new Error(response.body)
                                }
                            })
                    })
            })
    })


    test.each(no_password)('reset_password no pass %# %s', async (user) => {
        return supertest(server).post('/auth/register').send({...user, password: 'Welcome1!'})
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(async response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        return supertest(server).post('/auth/reset_password').send({email: user.email, password: null, token})
                            .then(response => {
                                expect(response.badRequest).toBeTruthy()
                                expect(response.status).toBe(400)
                                expect(response.body).toBe('password is required')
                            })
                    })
            })
    })


    test.each(no_email)('reset_password no email %# %s', async (user) => {
        return supertest(server).post('/auth/register').send({...user,email:user.password + 'fake@email.com'})
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email:user.password + 'fake@email.com'})
                    .then(async response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        return supertest(server).post('/auth/reset_password').send({email: null, password: user.password, token})
                            .then(response => {
                                expect(response.badRequest).toBeTruthy()
                                expect(response.status).toBe(400)
                                expect(response.body).toBe('email is required')
                            })
                    })
            })
    })

    test.each(ok_to_register)('reset_password no name %# %s', async (user) => {
        return supertest(server).post('/auth/register').send(user)
            .then(async () => {
                return supertest(server).post('/auth/forgot_password').send({email: user.email})
                    .then(async response => {
                        expect(response.badRequest).not.toBeTruthy()
                        const token = (mockedNodemailer.sendMail.mock.calls[0][0] as any).context.token
                        return supertest(server).post('/auth/reset_password').send({email: user.email, password: user.password, token})
                            .then(response => {
                                if (!response.badRequest) {
                                    expect(response.status).toBe(200)
                                } else {
                                    throw new Error(response.body)
                                }
                            })
                    })
            })
    })
})

describe('GET POST /auth/*', () => {
    test('* OK', async () => {
        return supertest(server).post('/auth/a').send(ok_to_register[0])
            .then(response => {
                if (!response.badRequest) {
                    expect(response.status).toBe(404)
                } else {
                    throw new Error(response.body)
                }
            })
    })

    test('* OK', async () => {
        return supertest(server).get('/auth/ab')
            .then(response => {
                expect(response.status).toBe(404)
            })
    })
})