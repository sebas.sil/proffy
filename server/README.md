# Proffy server

This project create the backend to Proffy system. This is responsable for receive the RESTFull requests.

### Get Started

1. Intall `Yarn`: https://yarnpkg.com/getting-started/install
2. After cloning this repository run `yarn install` to intall all the dependencies
3. Then run `migration:run` to create the database

### Unit test

To execute unit test on all functionalities run `yarn test --coverage`, it will run the test for all configurated tests. Or run individuality with: 
- `yarn test routes.users.test.ts` to execute test on users route
- `yarn test routes.classes.test.ts` to execute tests on classes route

The tests dataset is in [dates.classes.ts](./tests/dates.classes.ts) and [dates.ts](./tests/dates.ts)

### Deploy / Run

To execute this server, go to the server root folder and run `yarn start`

### Built With

[Visual Code 1.48.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [cors](https://www.npmjs.com/package/cors): Encharged to specify to the browser the ways for a server allowing its resources to be accessed by a web page from a different domain (this case, from app and web)
- [express](https://www.npmjs.com/package/express): Web applications framework to create a server
- [knex](https://www.npmjs.com/package/knex): SQL query builder for databases. It's replaces the need for write a raw database query
- [sqlite3](https://www.npmjs.com/package/sqlite3): Database file based, the simplest way to save things
- [jest](https://jestjs.io/docs/en/getting-started.html): JavaScript Testing Framework

### directory layout structure

    .
    ├── src                 # Has all application components and codes
    │   ├── config          # Knex configuration file
    │   ├── controllers     # functions called from views to trigger the database
    │   ├── database        # database file (sqlite file), migrations and seeds
    │   ├── exceptions      # custom exceptions to standardize messages
    │   ├── middlewares     # request inserseptors to verify something (mainly used to authenications)
    │   ├── Routes          # Rest path routes
    │   ├── utils           # Utilities like small functions and converters
    │   └── server.ts       # starting point of this applications
    ├── test                # Unit test folder (Jest)
    └── ...                 # others app files to configure this repository and project

### Running example images

<table>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/server_get_total_conn.png" alt="connections resource GET" width="720" />
                <figcaption>connections resource GET total connections</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/server_create_class.png" alt="classes resource POST" width="720" />
                <figcaption>classes resource POST to create a new class</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/server_list_class.png" alt="classes resource GET" width="720" />
                <figcaption>classes resource GET the classes list with mandatory filters</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/server_create_connection.png" alt="connections resource POST" width="720" />
                <figcaption>connections resource POST and increment the total connections</figcaption>
            </figure>
        </td>
    </tr>
</table>