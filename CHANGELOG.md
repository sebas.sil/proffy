# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Teachers can give more than one class
- mobile deeplink to reset password
- mobile favorites by loggedin user
- bew better css integrations (make simple files) and import contitions (dont blink)

## [2.0.0]

### Added

- user registration and authentication with a JWT token
- reset password mechanism with mail sending
- self registration
- authenicated user information (profile)

### Removed

- expo dependencies

- [Full Changelog](https://gitlab.com/sebas.sil/proffy/compare/1.0.0...2.0.0)

## [1.0.0]

### Added
- Favorites functionlity: The user can favorite a class and view it on a separate screen
- Teacher List: User can search for classes with required filters