# Proffy web

This project create the frontend (web) to Proffy system. This is responsable for send the RESTFull requests and rendering the webpage.

### Get Started

1. Intall `Yarn`: https://yarnpkg.com/getting-started/install
2. After cloning this repository run `yarn install` to intall all the dependencies

### Deploy / Run

To execute this frontend, go to the web root folder and run `yarn start`

### Built With

[Visual Code 1.48.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [axios](https://www.npmjs.com/package/axios): HTTP client
- [react](https://www.npmjs.com/package/react): JavaScript library for creating user interfaces.
- [Proffy backend](../server/README.md): Proffy backend server 

### directory layout structure

    .
    ├── public                                        # All items that are public and all users can view with abrowser GET
    ├── src                                           # Has all application components and codes
    │   ├── assets                                    # application resources folder
    │   │   └── images                                # images folder
    │   │       ├── icons                             # icons folder
    │   ├── components                                # shared components. A component is 'shared' whe it is used in more than one page
    │   │   ├── Checkbox                              # represent an HTML checkbox element
    │   │   ├── Input                                 # represent an HTML input text element
    │   │   ├── interfaces                            # interfaces used in more and one place
    │   │   ├── PageHeader                            # complet HTML element representing an entire header page with name, description and children
    │   │   ├── Select                                # represent an HTML compobox element
    │   │   ├── Success                               # represent an HTML success page
    │   │   ├── TeacherItem                           # complex HTML element representing an entire class card to show class atributes to the user
    │   │   └── TextArea                              # represent an HTML text area element
    │   ├── contex                                    # keep informations shared between pages
    │   ├── pages                                     # nagevation pages of the aplication
    │   │   ├── Forgot                                # Recover password page (must be unauthenticated)
    │   │   ├── Landing                               # Home page (must be authenticated)
    │   │   ├── Login                                 # Login page (must be unauthenticated)
    │   │   ├── Profile                               # Profile page (must be authenticated)
    │   │   ├── Register                              # Self registration page (must be unauthenticated)
    │   │   ├── ResetPassword                         # Reset passowrd page (must be unauthenticated)
    │   │   ├── TeacherForm                           # Insert class page (must be authenticated)
    │   │   └── TeacherList                           # List and search class page (must be authenticated)
    │   ├── services                                  # HTTP client api
    │   ├── routes                                    # Routes of the aplication (what happen when an user click on a navigation link)
    │   ├── App.tsx                                   # main component encapsulating the application
    │   ├── index.tsx                                 # starting point of the application
    │   └── ...                                       # others application files to configure this service
    └── ...                                           # others application files to continure this repository and project

### Running example images

<table>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_login.png" alt="Login page" width="480" />
                <img src=".gitimages/web_mob_login.png" alt="Login page" width="180" />
                <figcaption>Login page with basic authenticatin form (unauthenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_forgot.png" alt="Forgot password page" width="480" />
                <img src=".gitimages/web_mob_forgot.png" alt="Forgot password page" width="180" />
                <figcaption>Forgot page to receive the reset password e-mail (unauthenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_register.png" alt="Self registration page" width="480" />
                <img src=".gitimages/web_mob_register.png" alt="Self registration page" width="180" />
                <figcaption>Self registration page with simple form to create an account (unauthenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_reset.png" alt="Reset password page" width="480" />
                <img src=".gitimages/web_mob_reset.png" alt="Reset password page" width="180" />
                <figcaption>Reset password page (unauthenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_profile.png" alt="Profile page" width="480" />
                <img src=".gitimages/web_mob_profile.png" alt="Profile page" width="180" />
                <figcaption>Profile page with loggedin user information to update (authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_home.png" alt="Home page" width="480" />
                <img src=".gitimages/web_mob_home.png" alt="Home page" width="180" />
                <figcaption>Home page with the total connections (authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_search.png" alt="Search page" width="480" />
                <img src=".gitimages/web_mob_search.png" alt="Search page" width="180" />
                <figcaption>Search page with basic search form filter and results (authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_give.png" alt="GiveClass page" width="480" />
                <img src=".gitimages/web_mob_give.png" alt="GiveClass page" width="180" />
                <figcaption>GiveClass page with basic form to create a class (authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
</table>
