# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0]

### Added

- Enduser authentication with a JWT token
- Context to know if an object was modified by another screen
- recovover password page
- profile update information page
- self register page

### Changed
- Landing page (home screen) with loggedin user informations


- [Full Changelog](https://gitlab.com/sebas.sil/proffy/compare/1.0.0...2.0.0)

## [1.0.0]

### Added
- Favorites functionlity: The user can favorite a class and view it on a separate screen
- Teacher List: User can search for classes with required filters