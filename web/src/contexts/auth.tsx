import React, { createContext, useState, useEffect, useContext } from 'react'
import * as auth from '../services/api'
import { useMessages } from './messages'
const TAG = 'AUTH'
/**
 *
 */
interface AuthContextData {
    signed: boolean
    user: auth.IUser
    signIn(email: string, password: string, remember?: boolean): Promise<void>
    signOut(): Promise<void>
    register(name: string, email: string, password: string, remember?: boolean): Promise<void>
    reset(email: string, token: string, password: string, remember?: boolean): Promise<void>
    forgot(email: string): Promise<void>
    getTotalConnections(): Promise<number>
    update(user: auth.IClass): Promise<auth.IClass>
    update(user: auth.IUser): Promise<auth.IUser>
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData)

const AuthProvider: React.FC = ({ children }) => {

    const [user, setUser] = useState<auth.IUser>({} as auth.IUser)
    const { setTexts, setTitle } = useMessages()

    useEffect(() => {
        let user = sessionStorage.getItem('@Proffy:user') as string
        let token = sessionStorage.getItem('@Proffy:token') as string
        
        if(!user || !token){
            user = localStorage.getItem('@Proffy:user') as string
            token = localStorage.getItem('@Proffy:token') as string
        }

        auth.setAuthToken(token)
        if(user){
            setUser(JSON.parse(user))
        }
    }, [])

    async function signIn(email: string, password: string, remember = false) {
        console.log(TAG, 'sign in', email)
        auth.signIn(email, password).then((res) => {
            const { user, token } = res
            updateStores(user, remember, token)
        }).catch(console.log)
    }

    async function signOut() {
        console.log(TAG, 'sign out')
        localStorage.clear()
        sessionStorage.clear()
        updateStores({} as auth.IUser)
    }

    async function register(name: string, email: string, password: string, remember = false) {
        console.log(TAG, 'register', email)
        auth.register(name, email, password).then((res) => {
            const { user, token } = res
            console.log(TAG, 'register', user, token)
            setTitle('Cadastro concluído')
            setTexts(['Agora você faz parte da plataforma da Proffy.', 'Tenha uma ótima experiência.'])
            updateStores(user, remember, token)
        }).catch(console.log)
    }

    async function reset(email: string, password: string, token: string, remember = false): Promise<void> {
        console.log(TAG, 'reset', email)
        auth.resetPassword(email, password, token).then((res) => {
            const { user, token } = res
            console.log(TAG, 'register', user, token)
            updateStores(user, remember, token)
            auth.setAuthToken(token)
            setTitle('Redefinição concluída!')
            setTexts(['Boa, agora é só aproveitar os estudos.'])
        }).catch(console.log)
    }

    async function forgot(email: string): Promise<void> {
        console.log(TAG, 'forgot', email)
        auth.forgotPassword(email).then((res) => {
            setTitle('Redefinição enviada!')
            setTexts(['Boa, agora é só checar o e-mail que foi enviado para você redefinir sua senha e aproveitar os estudos.'])
        }).catch(console.log)
    }

    async function getTotalConnections(): Promise<number> {
        console.log(TAG, 'total connections')
        return auth.getTotalConnections().then((res) => {
            const { total } = res
            return total
        }).catch(e => {
            console.log(TAG, 'total connections', e)
            return 0
        })
    }

    async function update(user: auth.IUser): Promise<auth.IUser>
    async function update(user: auth.IClass): Promise<auth.IClass>

    async function update(input: any): Promise<any> {
        console.log(TAG, 'update')
        return auth.update(input).then((res)=>{
            setTitle('Atualização realizada!')
            setTexts([])
            if('cost' in input){
                user.class = input
            }
            const tokenL = localStorage.getItem('@Proffy:token') as string
            const tokenS = sessionStorage.getItem('@Proffy:token') as string

            updateStores(user, tokenL ? true : false, tokenS)
            return res
        })
    }

    async function updateStores(user:auth.IUser, remember?:boolean, token?:string){
        if (remember) {
            localStorage.setItem('@Proffy:user', JSON.stringify(user))
            localStorage.setItem('@Proffy:token', token as string)
        }

        sessionStorage.setItem('@Proffy:user', JSON.stringify(user))
        sessionStorage.setItem('@Proffy:token', token as string)

        auth.setAuthToken(token as string)
        
        setUser(user)
    }

    return (
        <AuthContext.Provider value={{ signed: Object.keys(user).length > 0, user, signIn, signOut, register, reset, forgot, getTotalConnections, update }}>
            {children}
        </AuthContext.Provider>
    )
}

const useAuth = () => {
    return useContext(AuthContext)
}

export { AuthProvider, useAuth }