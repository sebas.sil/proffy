import React, { createContext, useState, useContext } from 'react'

interface MessagesData {
    texts: Array<string>
    title: string
    setTexts(txts: Array<string>): void
    setTitle(txt: string): void
}

const MessagesContext = createContext<MessagesData>({} as MessagesData)

const MessagesProvider: React.FC = ({ children }) => {

    const [texts, setTexts] = useState<Array<string>>([])
    const [title, setTitle] = useState<string>('')

    return (
        <MessagesContext.Provider value={{ texts, setTexts, title, setTitle }}>
            {children}
        </MessagesContext.Provider>
    )
}

const useMessages = () => {
    return useContext(MessagesContext)
}

export { MessagesProvider, useMessages }