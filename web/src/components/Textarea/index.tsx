import React, { TextareaHTMLAttributes } from 'react'


/**
 * Interface to force parameters use and type
 */
interface TextAreaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
    /**
     * component unique name
     * This name is used to send the value when submited
     */
    name:string
    /**
     * component label
     * This label is used to show to the user and this component is rendered
     */
    label:string,
}

/**
 * Component to be reused by many pages.
 * It represents a Text area HTML component.
 * If this component modufy, all occurrences of it will by modified too
 */
const TextArea:React.FC<TextAreaProps> = ({ name, label, ...rest }) => {
    import(__dirname + '/styles.css')
    return (
        <div className="textarea-block">
            <label htmlFor={name}>{label}</label>
             <textarea id={name} {...rest} />
        </div>
    )
}

export default TextArea