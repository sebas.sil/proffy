import React, { InputHTMLAttributes } from 'react'

/**
 * Interface to force parameters use and type
 */
interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    /**
     * component unique name
     * This name is used to send the value when submited
     */
    name: string
    /**
     * component label
     * This label is used to show to the user and this component is rendered
     */
    label: string,
}

/**
 * Component to be reused by many pages.
 * It represents a Input text HTML component.
 * If this component modify, all occurrences of it will by modified too
 */
const Checkbox: React.FC<InputProps> = ({ name, label, ...rest }) => {

    import(__dirname + '/styles.css')
    return (
        <div className="check-block">
            <input type="checkbox" name={name} value="1" {...rest} />
            <label htmlFor={name}>{label}</label>
        </div>
    )
}

export default Checkbox