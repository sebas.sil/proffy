import React from 'react'

import WhatsIcon from '../../assets/images/icons/whatsapp.svg'

import api, {IUser} from '../../services/api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-regular-svg-icons'

/**
 * Interface to force parameters use and type
 */
interface ITeacher {
    item: IUser
}

/**
 * Cart component to represent a class with teacher, price and schedules
 * @param item, IClass - required - class item to be shown to the user 
 */
const TeacherItem: React.FC<ITeacher> = ({ item }) => {

    import(__dirname + '/styles.css')
    /**
     * Add a new conection count calling the connections resource
     * Called by 'contact' button
     */
    function addConnection() {
        api.post('connections', {
            class_id: item.id
        }).catch(() => alert('Um erro ocorreu ao realizar a conexao'))
    }

    /**
     * convert number to string week day
     * @return {string} weenk day name
     */
    function toWeekDay(v:number):string {
        switch (v) {
            case 0:
                return 'Domingo'
            case 1:
                return 'Segunda'
            case 2:
                return 'Terça'
            case 3:
                return 'Quarta'
            case 4:
                return 'Quinta'
            case 5:
                return 'Sexta'
            case 6:
                return 'Sabado'
            default:
                return '?'
        }
    }

    return (
        <article className="teacher-item">
            <header>
                {item.avatar ? 
                    <img src={item.avatar} alt='' /> :
                    <FontAwesomeIcon icon={faUser} className='avatar-svg' />
                }
                <div>
                    <strong>{item.name}</strong>
                    <span>{item.class.subject}</span>
                </div>
            </header>
            <p>{item.bio}</p>
            {item.class.schedule &&
                    <div className='teacher-item-schedule-content'>
                        <div className='teacher-item-schedule-header'><span>Dia</span><span>Horário</span></div>
                        {item.class.schedule.map((s,i) => {
                            return <div key={i} className='teacher-item-schedule'>
                                <div className='teacher-item-week_day'><span>{toWeekDay(s.week_day)}</span></div>
                                <div className='teacher-item-from'><span>{s.from}</span></div>
                                <div className='teacher-item-to'><span>{s.to}</span></div>
                            </div>
                        })}
                    </div>
                }

            <footer>
                <p>
                    Preço/Hora
                    <strong>R$ {item.class.cost}</strong>
                </p>
                <a target="_blank" href={'https://wa.me/' + item.phone} onClick={addConnection} rel="noopener noreferrer">
                    <img src={WhatsIcon} alt="Contato whatsapp" />
                    Entre em contato
                </a>
            </footer>
        </article>
    )
}

export default TeacherItem