import React, { InputHTMLAttributes } from 'react'

/**
 * Interface to force parameters use and type
 */
interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    /**
     * component unique name
     * This name is used to send the value when submited
     */
    name:string
    /**
     * component label
     * This label is used to show to the user and this component is rendered
     */
    label?:string,

    /**
     * componente icon
     * This icon is used to show hide password
     */
    icon?:InputHTMLAttributes<HTMLImageElement>
}

/**
 * Component to be reused by many pages.
 * It represents a Input text HTML component.
 * If this component modify, all occurrences of it will by modified too
 */
const Input:React.FC<InputProps> = ({ name, label, children, ...rest }) => {
    import(__dirname + '/styles.css')
    return (
        <div className={(rest.className ? rest.className + " " : "") + "input-block" }>
            {label && <label htmlFor={name}>{label}</label>}
            <input type="text" placeholder={label} id={name} {...rest} />
            {children}
        </div>
    )
}

export default Input