import React, { SelectHTMLAttributes } from 'react'

/**
 * Interface to force parameters use and type
 */
interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
    /**
     * component unique name
     * This name is used to send the value when submited
     */
    name:string
    /**
     * component label
     * This label is used to show to the user and this component is rendered
     */
    label:string,
    /**
     * component values options
     * This options is used to show the dropdown values to the user and this component is rendered
     */
    options:Array<{
        /**
         * option unique name
         * This name is used to send the value when submited
         */
        value:string,
        /**
         * option label
         * This label is used to show to the user and this option is rendered
         */
        label:string
    }>
}

/**
 * Component to be reused by many pages.
 * It represents a Select box HTML component.
 * If this component modify, all occurrences of it will by modified too
 */
const Select:React.FC<SelectProps> = ({ name, label, options, ...rest }) => {
    import(__dirname + '/styles.css')
    return (
        <div className={(rest.className ? rest.className + " " : "") + "select-block" }>
            <label htmlFor={name}>{label}</label>
            <select id={name} {...rest}>
            <option value="" defaultValue="" hidden>Selecione</option>
                {options.map(o => {
                    return <option key={o.value} value={o.value}>{o.label}</option>
                })}
            </select>
        </div>
    )
}

export default Select