import React from 'react'
import { Link } from 'react-router-dom'

import LogoImg from '../../assets/images/logo.svg'
import { ReactComponent as Logo } from '../../assets/images/logo.svg'
import BackIcon from '../../assets/images/icons/back.svg'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import './styles.css'
/**
 * Interface to force parameters use and type
 */
interface PageHeaderProps {
    /**
     * Title to be shown to the user when this component is rendered
     */
    title: string,
    /**
     * Description
     * Optional description to be shown to the user when this component is rendered
     */
    description?: string
}

/**
 * Header component to be shown in all application page
 * @param title, string - required - Title of the header page
 * @param description, string - optional - Description of the header page
 * @param children, HTMLElement - optional - Children elements to be added to the header
 */
const PageHeader: React.FC<PageHeaderProps> = ({ title, description, children }) => {

    return (
        <header className="page-header">
            <div className="top-bar-container">
                <Link to="/">
                    <img src={BackIcon} alt="Voltar" />
                </Link>
                <img src={LogoImg} alt="" className="logo-img" />
            </div>

            <div className="header-content">
                <strong>{title}</strong>
                {description && <p>{description}</p>}
                {children}
            </div>
        </header>
    )
}

/**
 * Header component to be shown in all application page
 * @param title, string - required - Title of the header page
 */
const PageHeaderThin: React.FC<PageHeaderProps> = ({ title }) => {
    import(__dirname + '/styles1.css')
    return (
        <header className="page-header-thin">
            <Link to="/" className='page-back-thin'>
                <FontAwesomeIcon icon={faArrowLeft} />
            </Link>

            <div className="header-content-thin">
                <span>{title}</span>
            </div>
            <div className="header-logo-thin">
                <Logo className="logo-img1" />
            </div>
        </header>
    )
}

export { PageHeaderThin }
export default PageHeader