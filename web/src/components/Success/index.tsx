import React from 'react';
import { useMessages } from '../../contexts/messages';
import OKImage from '../../assets/images/ok.svg'
import { useHistory } from 'react-router-dom';

const Success = () => {
    const { texts, title } = useMessages()
    const { push } = useHistory();

    import(__dirname + '/styles.css')

    function goBack(){
        push('/')
    }

    return (
        <div className='page-success-container'>
            <div className='page-success-message'>
                <div className='page-success-header'>
                    <img src={OKImage} alt='' />
                </div>
                <h2>{title}</h2>
                <div className='page-success-texts'>
                    {texts.map((text, idx) => <p key={idx}>{text}</p>)}
                </div>
            </div>
            <div className='page-success-button'>
                <button onClick={goBack}>Voltar</button>
            </div>
        </div>
    )
}

export default Success