import React, { useState, FormEvent } from 'react'

import './styles.css'
import PageHeader from '../../components/PageHeader'
import Input from '../../components/Input'


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import { ReactComponent as Logo } from '../../assets/images/logo.svg'
import { useAuth } from '../../contexts/auth'

const Register = () => {

    import(__dirname + '/styles.css')

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [name, setName] = useState<string>('')
    const [showPassword, setShowPassword] = useState<boolean>(false)

    const { register } = useAuth()
    
    async function handleSubmit(e: FormEvent) {
        e.preventDefault()
        register(name, email, password).then()
    }

    function toggleEye() {
        setShowPassword(!showPassword)
    }

    return (
        <div className="page-register-content">
            <div className="page-register page-register-primary">
                <PageHeader title="Cadastro" description="Preencha os dados abaixo para começar." />

                <main>
                    <form onSubmit={handleSubmit}>
                        <fieldset className="inputs">
                            <legend>Dados de cadastro</legend>
                            <Input name="name" required value={name} placeholder="Nome completo" onChange={e => setName(e.target.value)} />
                            <Input name="email" required value={email} type='email' placeholder="E-mail" onChange={e => setEmail(e.target.value)} />
                            <Input name="password" required value={password} placeholder='Senha' type={showPassword ? 'text' : 'password'} onChange={e => setPassword(e.target.value)}>
                                <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className='icon' onClick={toggleEye} />
                            </Input>

                            <button type="submit">
                                Concluir cadastro
                            </button>

                        </fieldset>

                    </form>
                </main>
            </div>
            <div className="page-register-secondary">
                <Logo />
                <p>Sua plataforma de estudos online.</p>
            </div>
        </div>
    )
}

export default Register