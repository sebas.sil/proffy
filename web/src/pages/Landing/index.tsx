import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as Logo } from '../../assets/images/logo.svg'
import LangingImg from '../../assets/images/landing.svg'
import StudyIcon from '../../assets/images/icons/study.svg'
import GiveClassesIcon from '../../assets/images/icons/give-classes.svg'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { faPowerOff, faHeart } from '@fortawesome/free-solid-svg-icons'
import { useAuth } from '../../contexts/auth'

/**
 * Application home page
 */
function Landing() {

    // TODO is it making the screen blinking?
    import(__dirname + '/styles.css')

    const { user, signOut, getTotalConnections } = useAuth()
    const [total, setTotal] = useState(0)

    useEffect(() => {
        getTotalConnections().then(setTotal)
    }, [getTotalConnections])

    return (
        <div className='page-langing'>

            <div className='page-langing-primary'>
                    <section className='page-langing-nav'>
                        <div className='page-langing-avatar'>
                        <Link to='profile'>
                            <div className='page-langing-avatar-img'>
                                {user.avatar ? 
                                    <img src={user.avatar} alt='' /> :
                                    <FontAwesomeIcon icon={faUser} />
                                }
                                </div>
                                <p>{user.name}</p>
                            </Link>
                        </div>
                        <div className='page-langing-signout'>
                            <FontAwesomeIcon icon={faPowerOff} className='page-langing-siginout-img' onClick={signOut} />
                        </div>
                    </section>
                    <section className='page-langing-header'>
                        <div className='page-langing-logo'>
                        <Logo />
                            <p>Sua plataforma de estudos online.</p>
                        </div>
                        <div className='page-langing-img'>
                            <img src={LangingImg} alt=''/>
                        </div>
                    </section>
            </div>
            <div className='page-langing-secondary'>
                <div className='page-langing-secondary-msg'>
                    <p>Seja bem vindo.</p>
                    <p>O que deseja fazer?</p>
                </div>
                <div className="page-langing-buttom-container">
                    <Link to="/study" className="study">
                        <img src={StudyIcon} alt="Estudar" />Estudar
                    </Link>
                    <Link to="/give-classes" className="give-classes">
                        <img src={GiveClassesIcon} alt="Dar aulas" />Dar aulas
                    </Link>
                </div>

                <span className="page-langing-total-connections">
                    Total de {total} conexões realizadas <FontAwesomeIcon icon={faHeart} />
                </span>

            </div>
            {/*
            <div className='container'>
                <div className='logo-container'>
                    <img src={LogoImg} alt='' />
                    <h2>Sua plataforma de estudos online</h2>
                </div>

                <img src={LangingImg} alt='' className='hero-image' />

                <div className='buttom-container'>
                    <Link to='/study' className='study'>
                        <img src={StudyIcon} alt='Estudar' />Estudar
                    </Link>
                    <Link to='/give-classes' className='give-classes'>
                        <img src={GiveClassesIcon} alt='Dar aulas' />Dar aulas
                    </Link>
                </div>

                <span className='total-connections'>
                    Total de {totalConnections} conexões realizadas <img src={PurpleHeartIcon} alt='' />
                </span>
            </div>
            */}
        </div>
    )
}

export default Landing