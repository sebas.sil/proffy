import React, { useState, FormEvent } from 'react'

import './styles.css'
import PageHeader from '../../components/PageHeader'
import Input from '../../components/Input'


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'
import { ReactComponent as Logo } from '../../assets/images/logo.svg'
import { useAuth } from '../../contexts/auth'
import { useHistory, useParams } from 'react-router-dom'

const ResetPassword = () => {

    import(__dirname + '/styles.css')

    const [password, setPassword] = useState<string>('')
    const [showPassword, setShowPassword] = useState<boolean>(false)

    const { reset } = useAuth()
    const { push } = useHistory()
    const { token, email } = useParams()

    async function handleSubmit(e: FormEvent) {
        e.preventDefault()
        reset(email, password, token)
        push('/success')
    }

    function toggleEye() {
        setShowPassword(!showPassword)
    }

    return (
        <div className='page-reset-content'>
            <div className='page-reset page-reset-primary'>
                <PageHeader title='Redefiniar Senha' description='Preencha os dados abaixo para redefinir sua senha.' />

                <main>
                    <form onSubmit={handleSubmit}>
                        <fieldset className='inputs'>
                            <legend>Redefiniar Senha</legend>
                            <Input name='password' value={password} placeholder='Nova Senha' type={showPassword ? 'text' : 'password'} onChange={e => setPassword(e.target.value)}>
                                <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className='icon' onClick={toggleEye} />
                            </Input>

                            <button type='submit'>
                                Redefinir Senha
                            </button>

                        </fieldset>

                    </form>
                </main>
            </div>
            <div className='page-reset-secondary'>
                <Logo />
                <p>Sua plataforma de estudos online.</p>
            </div>
        </div>
    )
}

export default ResetPassword