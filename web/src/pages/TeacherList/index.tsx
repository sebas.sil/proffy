import React, {useState, FormEvent} from 'react'

import {PageHeaderThin} from '../../components/PageHeader'
import TeacherItem from '../../components/TeacherItem'
import Input from '../../components/Input'
import Select from '../../components/Select'
import api, {IUser} from '../../services/api'
import {faFilter, faAngleDown, faAngleUp} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

/**
 * Page to search and list the classes and its content
 */
const TeacherList = () => {

    import(__dirname + '/styles.css')

    const [week_day, setWeekDay] = useState<number>()
    const [subject, setSubject] = useState<string>()
    const [time, setTime] = useState<string>()
    const [teachers, setTeachers] = useState<IUser[]>([])
    const [showFilter, setShowFilter] = useState(true)

    function handleSubmit(event: FormEvent) {
        event.preventDefault()
        api.get('/classes', {
            params: {
                week_day,
                subject,
                time
            }
        })
            .then(res => {
                setTeachers(res.data)
                setShowFilter(false)
            }).catch(e => console.log(e))
    }

    return (
        <div className="page-classes">
            <div className="page-classes-primary">
                <PageHeaderThin title='Estudar' />
                <div className="page-classes-avatar">
                    <div className="page-classes-avatar-content">
                        <p className="page-classes-avatar-name">Que incrível que você quer dar aulas.</p>
                        <p className="page-classes-avatar-subject">O primeiro passo, é preencher esse formulário de inscrição.</p>
                    </div>
                </div>
            </div>
            <div className="page-classes-secondary">

                <form className="page-classes-search-teachers" onSubmit={handleSubmit}>
                    <fieldset>
                        <legend>
                            <div className="page-classes-avatar-filter">
                                <FontAwesomeIcon icon={faFilter} />
                                <p>Filtrar por dia, hora e matéria</p>
                                <FontAwesomeIcon icon={showFilter?faAngleUp:faAngleDown} onClick={() => setShowFilter(!showFilter)} />
                            </div>
                        </legend>
                        <div className={'form-fields ' + (showFilter?'':'hide-filter')} >
                            <Select className='subject' name="subject" label="Matéria" options={[
                                {value: 'artes', label: 'Artes'},
                                {value: 'biologia', label: 'Biologia'},
                                {value: 'portugues', label: 'Portugues'},
                                {value: 'matematica', label: 'Matematica'}]}
                                onChange={e => setSubject(e.target.value)} />
                            <Select className='day' name="week_day" label="Dia da Semana" options={[
                                {value: '0', label: 'Domingo'},
                                {value: '1', label: 'Segunda-feira'},
                                {value: '2', label: 'Terça-feira'},
                                {value: '3', label: 'Quarta-feira'},
                                {value: '4', label: 'Quinta-feira'},
                                {value: '5', label: 'Sexta-feira'},
                                {value: '6', label: 'Sábado'}]}
                                onChange={e => setWeekDay(Number(e.target.value))} />
                            <Input className='time' name="time" label="Horas" type="time" onChange={e => setTime(e.target.value)} />
                            <button type="submit">Buscar</button>
                        </div>
                    </fieldset>
                </form>

                {teachers.length > 0 &&
                    <main>
                        {teachers.map(t => {
                            return <TeacherItem key={t.id} item={t} />
                        })}
                    </main>
                }
                <div className='page-classes-end'>
                    Estes são todos os resultados
                </div>
            </div>
        </div>
    )
}

export default TeacherList