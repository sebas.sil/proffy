import React, { useState, useEffect } from 'react'
import { ReactComponent as Logo } from '../../assets/images/logo.svg'

import Input from '../../components/Input'
import Checkbox from '../../components/Checkbox'
import { useAuth } from '../../contexts/auth'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons'

/**
 * Application authentication page
 */
function Login() {

    // TODO is it making the screen blinking?
    import(__dirname + '/styles.css')

    const { signIn } = useAuth()

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [remember, setRememeber] = useState<boolean>(false)
    const [showPassword, setShowPassword] = useState<boolean>(false)
    const [valid, setValid] = useState<boolean>(false)


    useEffect(() => {
        let v = false
        if(email && password){
            v = true
        }
        setValid(v)
    }, [email, password])
    // FIXME why are you orinting twice?
    // R: caused by React.StrictMode in index.js. Could I remove it?
    //console.log(TAG, 'signed', signed)

    async function handleSignIn(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        //const form = e.target as HTMLFormElement
        //console.log(form.checkValidity())
        signIn(email, password, remember)
    }

    function toggleEye(){
        setShowPassword(!showPassword)
    }

    return (
        <div className='page-login'>
            <div className='page-login-box page-login-primary'>
                <div className='page-login-box-group'>
                    <Logo />
                    <h2>Sua plataforma de estudos online.</h2>
                </div>
            </div>
            <div className='page-login-box page-login-secondary'>
                <div className='page-login-form-header'>
                    <span>Fazer login</span><a href='/register'>criar uma conta</a>
                </div>
                <form onSubmit={handleSignIn}>
                    <fieldset className='page-login-inputs'>
                        <Input required type='email' name='email' value={email} onChange={e => setEmail(e.target.value)} data-parse='lowercase'/>
                        <Input required name='password' value={password} type={showPassword ? 'text' : 'password'} onChange={e => setPassword(e.target.value)}>
                            <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className='icon' onClick={toggleEye} />
                        </Input>
                    </fieldset>
                    <div className='page-login-extends'>
                        <Checkbox name='rememberme' label='Lembre-me' checked={remember} onChange={e => setRememeber(e.target.checked)} />
                        <a href='/forgot'>esqueci minha senha</a>
                    </div>
                    <button type='submit' disabled={!valid}>Entrar</button>
                </form>
            </div>
        </div>
    )
}

export default Login