import React, { useState, FormEvent, useEffect } from 'react'
import { PageHeaderThin } from '../../components/PageHeader'

import { faUser } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Input from '../../components/Input'
import WarningIcon from '../../assets/images/icons/warning.svg'
import TextArea from '../../components/Textarea'
import Select from '../../components/Select'
import { useAuth } from '../../contexts/auth'
import {ISchedule} from '../../services/api'

/**
 * Page to create a new class, teacher and schedules
 */
const TeacherForm = () => {

    import(__dirname + '/styles.css')
    
    const { user, update } = useAuth()

    const [schedule, setSchedule] = useState<ISchedule[]>([])
    const [whatsapp, setWhats] = useState<string>('')
    const [bio, setBio] = useState<string>('')
    const [subject, setSubject] = useState<string>('')
    const [cost, setCost] = useState<number>(0)

    useEffect(() => {
        const clazz = user.class
        if(clazz) {
            setSubject(clazz.subject)
            setCost(clazz.cost)
            setSchedule(clazz.schedule)
        }
        setWhats(user.phone)
        setBio(user.bio)
    }, [user])
    /**
     * Add a new empty schedule item to the array, called by 'add new item' button
     */
    function addSchedule() {
        setSchedule([...schedule, { id: schedule.length} as ISchedule])
    }

    /**
     * Update the value of the property name of a specific schedule item
     * Called by onChange of the inputs
     * @param key , number - schedule item unique ID
     * @param field , string - property name to seek
     * @param value , string - property value to be updated
     */
    function setSelecteditem(key: number, field: string, value: string) {
        const arr = schedule.map(s => {
            if (s.id === key) {
                return { ...s, [field]: value }
            }

            return s
        })
        setSchedule(arr)
    }

    /**
     * Post the request to create a class, teacher and schedule items.
     * Called by submit button
     * @param event , FormEvent - triggered event
     */
    function handleSubmit(event: FormEvent) {
        event.preventDefault()
        update({id:user.class?.id || 0, subject, cost, schedule }).then(r => {
            user.class = r
        })
    }

    return (
        <div className="page-teacher">
            <div className="page-teacher-primary">
                <PageHeaderThin title='Dar aulas' />
                <div className="page-teacher-avatar">
                    <div className="page-teacher-avatar-content">
                        <p className="page-teacher-avatar-name">Que incrível que você quer dar aulas.</p>
                        <p className="page-teacher-avatar-subject">O primeiro passo, é preencher esse formulário de inscrição.</p>
                    </div>
                </div>
            </div>
            <div className="page-teacher-secondary">
                <main>
                    <form onSubmit={handleSubmit}>
                        <fieldset className="inputs">
                            <legend>Seus Dados</legend>
                                <div className="page-teacher-form-avatar">
                                    {user.avatar ?
                                        <img src={user.avatar} alt='' /> :
                                        <FontAwesomeIcon icon={faUser} />
                                    }
                                <p>{user.name}</p>
                                <Input name="whatsapp" label="Whatsapp" value={whatsapp} onChange={e => { setWhats(e.target.value) }} />
                            </div>
                            <TextArea name="bio" value={bio} label="Biografia" onChange={e => { setBio(e.target.value) }} />
                        </fieldset>

                        <fieldset className="inputs">
                            <legend>Sobre a aula</legend>
                            <Select name="subject" label="Matéria" options={[
                                { value: 'artes', label: 'Artes' },
                                { value: 'biologia', label: 'Biologia' },
                                { value: 'portugues', label: 'Português' },
                                { value: 'matematica', label: 'Matemática' }]}
                                value={subject} onChange={e => { setSubject(e.target.value) }} />
                            <Input name="cost" value={cost} label="Custo" type="number" onChange={e => { setCost(Number(e.target.value)) }} />
                        </fieldset>


                        <fieldset className="page-teacher-form-availability">
                            <legend>Horarios Disponíveis
                                <button type="button" onClick={addSchedule}>+ Novo</button>
                            </legend>

                            {schedule && schedule.map(s => {
                                return (
                                    <div key={s.id} className="page-teacher-form-schedule-item">
                                        <Select name="week_day" value={s.week_day} label="Dia da Semana" options={[
                                            { value: '0', label: 'Domingo' },
                                            { value: '1', label: 'Segunda-feira' },
                                            { value: '2', label: 'Terça-feira' },
                                            { value: '3', label: 'Quarta-feira' },
                                            { value: '4', label: 'Quinta-feira' },
                                            { value: '5', label: 'Sexta-feira' },
                                            { value: '6', label: 'Sábado' }]}
                                            onChange={e => setSelecteditem(s.id, 'week_day', e.target.value)} className="schedule-item-day" />
                                        <Input name="from" value={s.from} label="Das" type="time" onChange={e => setSelecteditem(s.id, 'from', e.target.value)} className="schedule-item-from"/>
                                        <Input name="to" value={s.to} min={s.from} label="Até" type="time" onChange={e => setSelecteditem(s.id, 'to', e.target.value)} className="schedule-item-to"/>
                                    </div>
                                )
                            })}
                        </fieldset>

                        <footer>
                            <div className="page-teacher-form-warn">
                                <img src={WarningIcon} alt="Aviso importante" />
                                
                                <div className="page-teacher-form-txt">
                                    <span>Importante!</span>
                                    <span>Preencha todos os dados</span>
                                </div>
                            </div>
                            <button type="submit">
                                Salvar Cadastro
                            </button>
                        </footer>
                    </form>
                </main>
            </div>
        </div>
    )
}

export default TeacherForm