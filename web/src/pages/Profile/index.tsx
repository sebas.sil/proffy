import React, { useState, useEffect, FormEvent } from 'react'
import { PageHeaderThin } from '../../components/PageHeader'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import WarningIcon from '../../assets/images/icons/warning.svg'
import { useAuth } from '../../contexts/auth'
import Input from '../../components/Input'
import TextArea from '../../components/Textarea'
import { useHistory } from 'react-router-dom'
import { IUser } from '../../services/api'

const Profile = () => {
    import(__dirname + '/styles.css')

    const { user, update } = useAuth();
    const { push } = useHistory()
    const [email, setEmail] = useState<string>('')
    const [name, setName] = useState<string>('')
    const [phone, setPhone] = useState<string>('')
    const [bio, setBio] = useState<string>('')
    const [avatar, setAvatar] = useState<string>('')

    useEffect(() => {
        setEmail(user.email)
        setName(user.name)
        setPhone(user.phone)
        setBio(user.bio)
        setAvatar(user.avatar)
    }, [user])

    async function handleSubmit(e: FormEvent) {
        e.preventDefault()
        await update({ id: user.id, name, email, phone, bio } as IUser)
        push('/')
    }

    return (
        <div className="page-profile">
            <div className="page-profile-primary">
                <PageHeaderThin title='Meu Perfil' />
                <div className="page-profile-avatar">
                    <div className="page-profile-avatar-img">
                        {avatar ?
                            <img src={avatar} alt='' /> :
                            <FontAwesomeIcon icon={faUser} />
                        }
                    </div>
                    <div className="page-profile-avatar-content">
                        <p className="page-profile-avatar-name">{user.name}</p>
                        <p className="page-profile-avatar-subject">{user.class?.subject}</p>
                    </div>
                </div>
            </div>
            <div className="page-profile-secondary">
                <main>
                    <form onSubmit={handleSubmit}>
                        <fieldset className="inputs">
                            <legend>Seus dados</legend>

                            <Input name="name" placeholder="Nome completo" label="Nome completo" value={name} onChange={e => setName(e.target.value)} />
                            <div className="page-profile-form-g1">
                                <Input name="email" placeholder="E-mail" label="E-mail" value={email} onChange={e => setEmail(e.target.value)} />
                                <Input name="whats" placeholder="Whatsapp" label="Whatsapp" value={phone} onChange={e => setPhone(e.target.value)} />
                            </div>
                            <TextArea name="bio" placeholder="Biografia" label="Biografia" value={bio} onChange={e => setBio(e.target.value)} />
                        </fieldset>
                        {/*
                            <fieldset>
                                <legend>Sobre a aula</legend>
                                <hr />
                                <Input name="subject" placeholder="Matéria" label="Matéria" />
                                <Input name="cost" placeholder="Custo por hora" label="Custo por hora" />
                            </fieldset>
                            <fieldset>
                                <div className='page-profile-form-g2'>
                                    <legend>Horários disponíveis</legend>
                                    <a href="/">+ Novo</a>
                                </div>
                                <hr />
                            </fieldset>
                            */}
                        <footer>
                            <p>
                                <img src={WarningIcon} alt="Aviso importante" />
                            Importante!<br />
                            Preencha todos os dados
                        </p>

                            <button type="submit">
                                Salvar Alterações
                        </button>
                        </footer>
                    </form>
                </main>
            </div>
        </div>
    )
}

export default Profile