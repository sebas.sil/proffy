import React, { useState, FormEvent } from 'react'

import './styles.css'
import PageHeader from '../../components/PageHeader'
import Input from '../../components/Input'

import { ReactComponent as Logo } from '../../assets/images/logo.svg'
import { useAuth } from '../../contexts/auth'
import { useHistory } from 'react-router-dom'

const ForgotPassword = () => {

    import(__dirname + '/styles.css')

    const [email, setEmail] = useState<string>('')
    
    const { forgot } = useAuth()
    const { push } = useHistory()

    async function handleSubmit(e: FormEvent) {
        e.preventDefault()
        forgot(email).then()
        push('/success')
    }

    return (
        <div className='page-forgot-content'>
            <div className='page-forgot page-forgot-primary'>
                <PageHeader title='Eita, esqueceu sua senha?' description='Não esquenta, vamos dar um jeito nisso.' />

                <main>
                    <form onSubmit={handleSubmit}>
                        
                        <fieldset className='inputs'>
                            <legend>Dados de cadastro</legend>
                            <Input name='email' required type='email' value={email} placeholder='E-mail' onChange={e => setEmail(e.target.value)} />

                            <button type='submit'>
                                Enviar
                            </button>

                        </fieldset>

                    </form>
                </main>
            </div>
            <div className='page-forgot-secondary'>
                <Logo />
                <p>Sua plataforma de estudos online.</p>
            </div>
        </div>
    )
}

export default ForgotPassword