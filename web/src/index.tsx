import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/**
 * Starting point of this aplication.
 * Render the App component inside 'root' div of /public/index.html
 */
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
