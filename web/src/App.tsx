import React from 'react'
import Routes from './routes'
import './assets/styles/global.css'
import { AuthProvider } from './contexts/auth'
import { MessagesProvider } from './contexts/messages'

/**
 * Aply the global style of this application and call the first route of Routes component
 */
function App() {
  return (
    <MessagesProvider>
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </MessagesProvider>
  );
}

export default App;
