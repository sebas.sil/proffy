import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Landing from '../pages/Landing'
import TeacherList from '../pages/TeacherList'
import TeacherForm from '../pages/TeacherForm'
import Login from '../pages/Login'
import Success from '../components/Success'
import Profile from '../pages/Profile'

/**
 * Routes thet user must be authenticated to archive
 */
const AppRoutes = () => {
    
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/register" exact component={Login}>
                    <Redirect to="/success" />
                </Route>
                <Route path="/study" exact component={TeacherList} />
                <Route path="/give-classes" exact component={TeacherForm} />
                <Route path="/success" exact component={Success} />
                <Route path='/landing' component={Landing} />
                <Route path='/profile' component={Profile} />
                <Route path="*">
                    <Redirect to="/landing" />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default AppRoutes