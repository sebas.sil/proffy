import React from 'react'
import AuthRoutes from './auth.routes'
import { useAuth } from '../contexts/auth'
import AppRoutes from './routes'

const TAG = 'ROUTES'
/**
 * Encharge to redirect an unauthenticated user to 
 * login page and prevent an unauthenticated user to access the app
 */
const Routes = () => {
    const { signed } = useAuth()
    console.log(TAG, 'route', signed)
    return (
        signed ? <AppRoutes /> : <AuthRoutes />
    )
}

export default Routes