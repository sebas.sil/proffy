import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Login from '../pages/Login'
import Register from '../pages/Register'
import ResetPassword from '../pages/ResetPassword'
import Success from '../components/Success'
import ForgotPassword from '../pages/ForgotPassword'

/**
 * Routes that the user can archive without authentication
 */
const AuthRoutes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/register" component={Register} />
                <Route path="/reset/:email/:token" component={ResetPassword} />
                <Route path="/success" component={Success} />
                <Route path="/forgot" component={ForgotPassword} />
                <Route path="/login" component={Login} />
                <Route path="*">
                    <Redirect to="/login" />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default AuthRoutes