import axios from 'axios'

export interface IConnection {
    total: number
}

/**
 * Express an availability of a class
 */
export interface IUser {
    id: number
    name: string
    avatar: string
    phone: string
    bio: string
    email: string
    password: string
    created_at: Date
    updated_at: Date
    class: IClass
}

export interface IAuthResponse {
    user: IUser,
    token: string
}

export interface IClass {
    id: number
    subject: string
    cost: number
    schedule: Array<ISchedule>
}

export interface ISchedule {
    id: number
    week_day: number
    from: number | string
    to: number | string
    class_id: any
}

/**
 * Encapsulates the HTTP client
 */
const api = axios.create({
    baseURL: 'http://192.168.25.11:3333'
})

/**
 * Calls the backend to authenticate an user
 * @param email , string - user login
 * @param password , string - user password
 */
async function signIn(email: string, password: string): Promise<IAuthResponse> {
    return api.post('/auth/authenticate', {
        email, password
    }).then(res => {
        const data = res.data
        return data as IAuthResponse
    })
}

/**
 * Create a new user
 * @param name , string - user display name
 * @param email , string - user e-mail (login)
 * @param password , string - user password
 */
async function register(name: string, email: string, password: string): Promise<IAuthResponse> {
    return api.post('/auth/register', {
        name, email, password
    }).then(res => {
        const data = res.data
        return data as IAuthResponse
    })
}

/**
 * Send e-mail redefinition 
 * @param email , string - user e-email (login)
 * @param password , string - user new password
 * @param token , string - user JWT token
 */
async function resetPassword(email: string, password: string, token: string): Promise<IAuthResponse> {
    return api.post('/auth/reset_password', { email, password, token }).then(res => {
        const data = res.data
        return data as IAuthResponse
    })
}

/**
 * Send e-mail redefinition 
 * @param email , string - user e-email (login)
 */
async function forgotPassword(email: string): Promise<void> {
    api.post('/auth/forgot_password', { email })
}

/**
 * Signed out an authenticated user
 */
async function signOut(): Promise<void> {
    // no need calls the backend
    return new Promise<void>(() => { })
}

/**
 * Get the total number og connections made between teachers and students
 */
async function getTotalConnections(): Promise<IConnection> {
    return api.get('/connections').then(res => {
        const data = res.data
        return data as IConnection
    })
}

function setAuthToken(token: string) {
    api.defaults.headers['Authorization'] = `Bearer ${token}`
}

function update(user:IUser):Promise<IUser>;
function update(classes:IClass):Promise<IClass>;


async function update(input: any): Promise<any> {
    if('cost' in input) {
        return api.post('/classes', input).then(res => {
            return res.data
        })
    } else {
        return api.post('/update', input).then(res => {
            return res.data
        })
    }
}

export { signIn, signOut, setAuthToken, register, resetPassword, forgotPassword, getTotalConnections, update }
export default api
