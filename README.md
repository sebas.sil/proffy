# Proffy

This project create the backend and frontend (web and mobile) to Proffy system.

Version 2.0 of web and mobile "Proffy" platform to bring students and teachers together on pandemic times. Created in React and React Native, initially proposed on NLW2 by Rocketseat (version 1.0) and let it by chalenge th eincrement to version 2.0.

Layout implemented by me, but created by Rocketseat avaliable on:
- [Proffy Mobile 2.0](https://www.figma.com/file/6TqsvYpQWwDZhuY3DpRWCN/Proffy-Mobile-2.0)
- [Proffy Web 2.0](https://www.figma.com/file/2r3Pd1GN2Knbkvr7LEyAmH/Proffy-Web-2.0)

Specification: 
- [Proffy 2.0](https://www.notion.so/Vers-o-2-0-Proffy-eefca1b981694cd0a895613bc6235970)

### Get Started

Each folder is a module: frontend, backend and mobile. Enter to read the README of each one and receive better explanetions

### Deploy / Run

Each modeule has its own node_module to be installed and then run `yarn start`

### Built With

[Visual Code 1.48.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [react](https://www.npmjs.com/package/react): JavaScript library for creating user interfaces.
- [react-native](https://www.npmjs.com/package/react-native): React's declarative UI framework to iOS and Android
- [knex](https://www.npmjs.com/package/knex): SQL query builder for databases. It's replaces the need for write a raw database query
- [sqlite3](https://www.npmjs.com/package/sqlite3): Database file based, the simplest way to save things
- [jest](https://jestjs.io/docs/en/getting-started.html): JavaScript Testing Framework

### directory layout structure

    .
    ├── .gitimages                 # repository images to show on README files
    ├── mobile                     # fontend mobile project (IOS and Android) using react-native and express
    ├── server                     # backend project using sqlite3 and knex
    ├── web                        # frontend web project using React
    └── ...                        # others app files to configure this repository and project

### Running example images

<table>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/server_create_class.png" alt="classes resource POST" width="480" />
                <figcaption>classes resource POST to create a new class</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/mobile_favorites.jpg" alt="Favorite screen" width="180" />
                <figcaption>Favorite screen with favorited classes</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/web_full_give.png" alt="GiveClass page" width="480" />
                <img src=".gitimages/web_mob_give.png" alt="GiveClass page" width="180" />
                <figcaption>GiveClass page with basic form to create a class</figcaption>
            </figure>
        </td>
    </tr>
</table>