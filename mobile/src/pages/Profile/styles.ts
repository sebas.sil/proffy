import {StyleSheet, Platform, StatusBar} from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({

    container: {
        flex: 1
    },

    scroll: {
        flex: 1
    },

    headerAvatarImg: {
        borderRadius: 60,
        height: 120,
        width: 120,
        marginTop: padding.lg * 2
    },

    headerAvatarSvg: {
        fontSize: 120,
        color: colors.primaryDark,
        marginTop: padding.lg * 2
    },

    headerAvatarName:{
        fontFamily: fonts.primary,
        fontSize: fonts.sm * 3,
        marginTop: padding.md,
        color: colors.colorTextPrimaryLight
    },

    headerAvatarSubject: {
        fontFamily: fonts.primary,
        fontSize: fonts.sm * 3,
        color: colors.colorTextPrimaryLight
    },

    primaryContainer: {
        alignItems: 'center',
        minHeight: (dimensions.fullHeight / 2) - padding.xl,
        backgroundColor: colors.primary
    },

    secondaryContainer: {
        alignItems: 'center'
    },

    bg: {
        position: 'absolute',
        top: padding.lg * 2,
        color: colors.colorTextPrimaryLight,
    },

    secondaryForm: {
        width: '90%',
        backgroundColor: 'white',
        height: '100%',
        marginTop: -padding.xl,
        borderRadius: 8,
        padding: padding.lg
    },

    secondaryFormLegend: {
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.lg,
        marginBottom: padding.lg,
        borderBottomWidth: 1,
        borderBottomColor: colors.background
    },

    input: {
        marginBottom: padding.md,
        backgroundColor: colors.background
    },

    textarea: {
        height: 300,
        textAlignVertical: 'top'
    },

    loginButton: {
        backgroundColor: '#04d361',
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    },

    loginButtonText: {
        color: 'white',
        fontSize: fonts.md,
        fontFamily: 'Archivo-Bold'
    },
    
})

export default styles