import React, {useState, useEffect} from 'react'
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native'
import PageHeader from '../../components/PageHeader'
import {useAuth} from '../../context/Auth'
import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons'
import Background from '../../assets/images/background-primary-2.svg'
import Input from '../../components/Input'
import {RectButton} from 'react-native-gesture-handler'

const Profile = () => {

    const {user, update} = useAuth()
    const [name, setName] = useState<string>('')
    const [phone, setPhone] = useState<string>('')
    const [bio, setBio] = useState<string>('')

    async function handleSubmit() {
        return update({ id: user.id, name, email: user.email, phone, bio })
    }

    useEffect(() => {
        setName(user.name)
        setPhone(user.phone)
        setBio(user.bio)
    }, [user])

    return (
        <View style={styles.container}>
            <PageHeader title='Meu Perfil' />
            <ScrollView style={styles.scroll}>
                <>
                    <View style={styles.primaryContainer}>
                        <Background style={styles.bg} />
                        <>
                            {
                                user.avatar ? <Image source={{uri: user.avatar}} style={styles.headerAvatarImg} /> : <Icon name='person-sharp' style={styles.headerAvatarSvg} />
                            }
                        </>
                        <Text style={styles.headerAvatarName}>{user.name}</Text>
                        <Text style={styles.headerAvatarSubject}>{user.class?.subject}</Text>
                    </View>
                    <View style={styles.secondaryContainer}>
                        <View style={styles.secondaryForm}>
                            <Text style={styles.secondaryFormLegend}>Seus Dados</Text>
                            <Input label='Nome completo' value={name} style={styles.input} onChangeText={setName} keyboardType='default' textContentType='name' />
                            <Input label='E-mail' value={user.email} editable={false} style={styles.input} keyboardType='email-address' textContentType='emailAddress' />
                            <Input label='Telefone' value={phone} style={styles.input} onChangeText={setPhone} keyboardType='phone-pad' textContentType='telephoneNumber' />
                            <Input label='Bio' value={bio} multiline={true} onChangeText={setBio} numberOfLines={6} style={[styles.input, styles.textarea]} keyboardType='default' textContentType='telephoneNumber' />
                            <RectButton>
                                <TouchableOpacity style={[styles.loginButton]} onPress={handleSubmit}>
                                    <Text style={styles.loginButtonText}>Atualizar</Text>
                                </TouchableOpacity>
                            </RectButton>
                        </View>
                    </View>
                </>
            </ScrollView>
        </View>
    )
}

export default Profile