import {StyleSheet, Dimensions} from 'react-native'

export const colors = {
    primaryLight: '#ebebf5',
    primary: '#8257e5',
    primaryDark: '#774DD6',
    secondary: '#04ba55',
    colorTextPrimaryLight: '#D4C2FF',
    colorTextPrimaryDark: '#6A6180',
    placeholder:'#9C98A6',
    disabled: '#DCDCE5',
    backgroundLight: '#FAFAFC',
    background: '#f2f2f2',
    backgroundDark: '#E6E6F0'
}

export const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
}

export const padding = {
    /**
     * 10
     */
    sm: 10,
    /**
     * 20
     */
    md: 20,
    /**
     * 30
     */
    lg: 30,
    /**
     * 40
     */
    xl: 40
}

export const fonts = {
    /**
     * 8
     */
    sm: 8,
    /**
     * 16
     */
    md: 16,
    /**
     * 32
     */
    lg: 32,
    /**
     * 64
     */
    xl: 64,
    /**
     * Poppins-Regular
     */
    primary: 'Poppins-Regular',
    /**
     * Poppins-Bold
     */
    primaryBold: 'Poppins-Bold',
    /**
     * Archivo-Regular
     */
    secondary: 'Archivo-Regular',
    /**
     * Archivo-Bold
     */
    secondaryBold: 'Archivo-Bold'
  }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: padding.xl
    },

    textPrimaryLight: {
        color: colors.colorTextPrimaryLight,
        fontFamily: 'Poppins-Regular',
        fontSize: fonts.md
    },

    textPrimaryDark: {
        color: colors.colorTextPrimaryDark,
        fontFamily: 'Poppins-Regular',
        fontSize: fonts.sm
    },

    logo: {
        color: colors.colorTextPrimaryLight
    },

    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    disabled: {
        backgroundColor: colors.disabled
    },

    label: {
        fontFamily: fonts.primary,
        fontSize: fonts.md,
        color: colors.placeholder
    }
})

export default styles