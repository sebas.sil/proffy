import {StyleSheet} from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({

    primaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center'
    },

    primaryContainerTitle: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.md * 3,
        marginBottom: padding.lg,
        fontFamily: fonts.primaryBold
    },

    primaryContainerDescription: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.sm * 3,
        width: dimensions.fullWidth * 0.7,
        fontFamily: fonts.primary
    },

    secondaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center'
    },

    secondaryTitle: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.sm * 3,
        fontFamily: fonts.primaryBold,
        marginBottom: padding.lg
    },

    eye: {
        position: 'absolute',
        bottom: (fonts.lg - 4) / 2,
        right: padding.lg / 2,
        fontSize: fonts.md * 1.5,
        color: colors.colorTextPrimaryDark
    },

    loginButton: {
        backgroundColor: '#04d361',
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginTop: padding.lg
    },

    loginButtonText: {
        color: 'white',
        fontSize: fonts.md,
        fontFamily: 'Archivo-Bold'
    },

    check: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: padding.sm
    },

    checkText: {
        color: colors.colorTextPrimaryDark
    },

})

export default styles