import React, {useState} from 'react'
import {View, Text} from 'react-native'

import Input from '../../components/Input'
import CheckBox from '@react-native-community/checkbox'

import styles from './styles'
import {RectButton} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import {StatusBar} from "react-native"
import {colors} from '../style_global'
import {useAuth} from '../../context/Auth'
import {useNavigation} from '@react-navigation/native'
/**
 * App home screen
 */
const Step1 = () => {

    const {register} = useAuth()
    const {navigate} = useNavigation()

    const [showPassword, setShowPassword] = useState<boolean>(false)
    const [password, setPassword] = useState<string>('')
    const [name, setName] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [remember, setRemember] = useState<boolean>(false)

    function toggleEye() {
        setShowPassword(!showPassword)
    }

    function handleGoNext() {
        register(name, email, password, remember)
    }

    return (
        <>
            <StatusBar backgroundColor={colors.background} barStyle="light-content" />
            <View style={[styles.primaryContainer]}>
                <Text style={[styles.primaryContainerTitle]}>Crie sua conta gratuíta</Text>
                <Text style={[styles.primaryContainerDescription]}>Basta preencher esses dados e você estará conosco.</Text>
            </View>
            <View style={[styles.secondaryContainer]}>
                <Text style={[styles.secondaryTitle]}>Como quer ser reconhecido?</Text>
                <Input placeholder='Nome completo' value={name} onChangeText={setName} keyboardType='default' textContentType='name'/>
                <Input placeholder='E-mail' value={email} onChangeText={setEmail} keyboardType='email-address' textContentType='emailAddress'/>
                <Input value={password} placeholder='Senha' onChangeText={setPassword} secureTextEntry={!showPassword} textContentType='password'>
                    <Icon name={showPassword ? 'eye-off-outline' : 'eye-outline'} style={[styles.eye]} onPress={toggleEye} />
                </Input>
                <View style={styles.check}>
                    <CheckBox name='rememberme' value={remember} onValueChange={setRemember} />
                    <Text style={styles.checkText}>Lembre-me</Text>
                </View>
                <RectButton style={styles.loginButton} onPress={handleGoNext}>
                    <Text style={styles.loginButtonText}>Concluir cadastro</Text>
                </RectButton>
            </View>
        </>
    )
}

export default Step1