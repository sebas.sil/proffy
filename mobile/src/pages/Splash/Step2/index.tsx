import React, {useEffect} from 'react'
import {View, Text} from 'react-native'
import Background from '../../../assets/images/background-primary-2.svg'
import AsyncStorageStatic from '@react-native-community/async-storage'

import {colors} from '../../style_global'
import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons'
import {BorderlessButton} from 'react-native-gesture-handler'
import {useNavigation} from '@react-navigation/native'

/**
 * App home screen
 */
const Step2 = () => {

    useEffect(() => {
        AsyncStorageStatic.setItem('first_time', '0').catch(console.log)
    }, [])

    const navigation = useNavigation()

    function handleGoNext() {
        navigation.navigate('Login')
    }

    return (
        <>
            <View style={[styles.container]}>
                <View style={[styles.primaryContainer]}>
                    <Icon name='tv-outline' style={styles.primaryImage} />
                    <Background style={styles.bg} />
                </View>
                <View style={[styles.secondaryContainer]}>
                    <View style={[styles.secondary]}>
                        <Text style={[styles.secondaryTitle]}>02.</Text>
                        <Text style={[styles.secondaryText]}>Ou dê aulas sobre o que você mais conhece.</Text>
                        <BorderlessButton onPress={handleGoNext} style={[styles.nextContainer]}>
                            <Icon name='arrow-forward' style={[styles.next, styles.nextDark]} />
                        </BorderlessButton>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Step2