import {StyleSheet} from 'react-native'
import {colors, dimensions, fonts, padding} from '../../style_global'

const styles = StyleSheet.create({

    container: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    bg: {
        position: 'absolute',
        color: colors.colorTextPrimaryLight
    },

    nextDark: {
        fontSize: fonts.lg,
        color: colors.colorTextPrimaryDark
    },

    nextContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },

    primaryImage: {
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.lg * 5
    },

    secondaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
        padding: padding.xl,
    },

    primaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center',
        alignItems: 'center'
    },

    secondary: {
        flex: 1
    },

    secondaryTitle: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.lg * 2,
        opacity: 0.3,
        marginTop: padding.xl
    },

    secondaryText: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.lg,
        marginTop: padding.lg,
        width: dimensions.fullWidth / 2.5
    }
})

export default styles