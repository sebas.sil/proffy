import React from 'react'
import {View, ImageBackground, Text} from 'react-native'
import Background from '../../../assets/images/background-primary-1.svg'

import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons'
import {BorderlessButton} from 'react-native-gesture-handler'
import {useNavigation} from '@react-navigation/native'

/**
 * App home screen
 */
const Step1 = () => {

    const navigation = useNavigation()

    function handleGoNext() {
        navigation.navigate('Step2')
    }

    return (
        <View style={[styles.container]}>
            <View style={[styles.primaryContainer]}>
                    <Icon name='book-outline' style={styles.primaryImage} />
                    <Background style={styles.bg} />
                </View>
            <View style={[styles.secondaryContainer]}>
                <View style={[styles.secondary]}>
                    <Text style={[styles.secondaryTitle]}>01.</Text>
                    <Text style={[styles.secondaryText]}>Encontre vários professores para ensinar você.</Text>
                    <BorderlessButton onPress={handleGoNext} style={[styles.nextContainer]}>
                        <Icon name='arrow-forward' style={[styles.next, styles.nextDark]} />
                    </BorderlessButton>
                </View>
            </View>
        </View>
    )
}

export default Step1