import React, {useEffect} from 'react'
import { View, ImageBackground, Text } from 'react-native'
import giveClassesBG from '../../assets/images/give-classes-background.png'

import stylesG from '../style_global'
import styles from './styles'
import Logo from '../../assets/images/logo.svg'
import Icon from 'react-native-vector-icons/Ionicons'
import {BorderlessButton} from 'react-native-gesture-handler'
import {useNavigation} from '@react-navigation/native'

/**
 * App home screen
 */
const Splash = () => {
    
    const navigation = useNavigation()

    function handleGoNext(){
        navigation.navigate('Step1')
    }

    useEffect(()=>{
        console.log('splash')
    }, [])
    
    return (
        <View style={[stylesG.container, styles.container]}>
            <ImageBackground resizeMode='contain' source={giveClassesBG} style={[styles.primaryContainer]}>
                <View style={styles.logoContainer}>
                    <Logo style={[styles.logo, stylesG.textPrimaryLight]} width={160} height={46} />
                    <Text style={[stylesG.textPrimaryLight]}>
                        Sua plataforma de estudos online.
                    </Text>
                </View>
                <BorderlessButton onPress={handleGoNext} style={[styles.nextContainer]}>
                    <Icon name='arrow-forward' style={[styles.next, styles.nextLight]} />
                </BorderlessButton>
            </ImageBackground>
        </View>
    )
}

export default Splash