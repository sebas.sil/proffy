import {StyleSheet} from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({

    container: {
        backgroundColor: colors.primary,
        flex: 1,
    },

    logoContainer: {
        width: 160
    },

    nextLight: {
        fontSize: fonts.lg,
        color: colors.colorTextPrimaryLight
    },

    nextContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },

    primaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default styles