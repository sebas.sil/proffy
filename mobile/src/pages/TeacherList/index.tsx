import React, {useState} from 'react'
import {View, Text, TextInput, ScrollView} from 'react-native'

import styles from './styles'
import PageHeader from '../../components/PageHeader'
import TeacherItem from '../../components/TeacherItem'
import api, {IUser} from '../../services/api'
import IClass from '../../components/interfaces/Iclass'
import Filter from '../../components/Filter'
import {useFavorites} from '../../context/Favorites'
import Icon from 'react-native-vector-icons/Ionicons'

/**
 * Screen to search and list the classes and its content
 */
const TeacherList = () => {

    const [isVisible, setIsVisible] = useState<boolean>(true)
    const [classes, setClasses] = useState<IClass[]>([])
    const {favorites, removeFromFavorites, addToFavorites} = useFavorites();

    function toggleFavorite(isFavorite:boolean, item:IUser) {
        if (isFavorite) {
            removeFromFavorites(item).catch(console.log)
        } else {
            addToFavorites(item).catch(console.log)
        }
    }

    /**
     * Send a request to the backend with selected filter
     * called by 'Filtrar' button on header. It some classe was found, the filter is hidden
     */
    async function submit(subject: string, weekDay: string, time: string): Promise<void> {
        setIsVisible(true)
        return api.get('/classes', {
            params: {
                week_day: weekDay,
                subject,
                time
            }
        }).then(res => {
            setClasses(res.data)
            if (res.data?.length > 0) {
                setIsVisible(false)
            }
        }).catch(e => console.log(e))
    }

    return (
        <View style={styles.container}>
            <PageHeader title='Estudar' />
            <ScrollView>
                <View style={styles.primaryContainer}>
                    <View style={styles.primaryTitleContainer}>
                        <Text style={styles.headerTitle}>Proffys Disponíveis.</Text>
                        <View style={styles.headerCountContainer}>
                            <Icon name='happy-outline' style={styles.headerTitleIcon} />
                            <Text style={styles.headerCountTxt}>{classes.length}</Text>
                        </View>
                    </View>
                    <Filter submit={submit} isVisible={isVisible} />
                </View>

                <View style={[styles.secondaryContainer, styles.teacherList]}>

                    {classes.map((u: IUser) => {
                        const isFavorite = favorites.findIndex((e:IUser) => e.id === u.id) != -1
                        return <TeacherItem key={u.id} item={u} favorited={isFavorite} haddleToggleFavorite={() => toggleFavorite(isFavorite, u)}/>
                    })}

                </View>
            </ScrollView>
        </View>
    )
}

export default TeacherList