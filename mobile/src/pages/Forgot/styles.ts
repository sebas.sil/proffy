import {StyleSheet, Platform, StatusBar} from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({

    container: {
        backgroundColor: colors.primary,
        flex: 1
    },

    bg: {
        position: 'absolute',
        color: colors.colorTextPrimaryLight
    },

    nextDark: {
        fontSize: fonts.lg,
        color: colors.colorTextPrimaryDark
    },

    nextContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },

    primaryImage: {
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.lg * 5
    },

    secondaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
        padding: padding.xl,
    },

    primaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primary
    },

    secondary: {
        flex: 1
    },

    secondaryTitle: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.lg,
        fontFamily: fonts.primaryBold
    },

    secondaryTitleHint: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.md
    },

    secondaryText: {
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.md * 1.5,
        marginTop: padding.lg,
        width: dimensions.fullWidth / 2.5
    },

    loginFormTitle: {
        marginVertical: padding.md
    },

    loginFormTitleHint: {
        marginBottom: padding.lg,
        marginTop: padding.sm
    },

    eye: {
        position: 'absolute',
        bottom: (fonts.lg - 4) / 2,
        right: padding.lg / 2,
        fontSize: fonts.md * 1.5,
        color: colors.colorTextPrimaryDark
    },

    loginButton: {
        backgroundColor: '#04d361',
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginVertical: padding.lg
    },

    loginButtonText: {
        color: 'white',
        fontSize: fonts.md,
        fontFamily: 'Archivo-Bold'
    },

})

export default styles