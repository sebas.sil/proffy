import React, {useState} from 'react'
import {View, CheckBox, Text, KeyboardAvoidingView} from 'react-native'
import Background from '../../assets/images/background-primary-1.svg'
import Logo from '../../assets/images/logo.svg'
import Input from '../../components/Input'
import styles from './styles'
import {RectButton} from 'react-native-gesture-handler'
import {useNavigation} from '@react-navigation/native'
import stylesG from '../style_global'
import {useAuth} from '../../context/Auth'

/**
 * App home screen
 */
const Forgot = () => {

    const { forgot } = useAuth()
    const {goBack} = useNavigation()

    const [email, setEmail] = useState<string>('')

    function handleForgot() {
        forgot(email).then(() => {
            goBack()
        })
    }

    return (
        <>
            
            <View style={[styles.container]}>
                <View style={[styles.primaryContainer]}>
                    <View style={{width: 170}}>
                        <Logo style={[styles.logo, stylesG.textPrimaryLight]} width={160} height={46} />
                        <Text style={[stylesG.textPrimaryLight]}>
                            Sua plataforma de estudos online.
                        </Text>
                    </View>
                    <Background style={styles.bg} />
                </View>
                <KeyboardAvoidingView style={[styles.secondaryContainer]} behavior='padding'>
                    <View style={[styles.secondary]}>
                        <View style={styles.loginFormTitle}>
                            <Text style={[styles.secondaryTitle]}>Esqueceu sua senha?</Text>
                        </View>
                        <View style={styles.loginFormTitleHint}>
                            <Text style={[styles.secondaryTitleHint]}>Não esquenta,</Text>
                            <Text style={[styles.secondaryTitleHint]}>vamos dar um jeito nisso.</Text>
                        </View>
                        <Input value={email} placeholder='E-mail' onChangeText={setEmail} keyboardType='email-address' textContentType='username'/>
                        <RectButton style={styles.loginButton} onPress={handleForgot}>
                            <Text style={styles.loginButtonText}>Entrar</Text>
                        </RectButton>
                    </View>
                </KeyboardAvoidingView>
            </View>
        </>
    )
}

export default Forgot