import React, { useState, useEffect } from 'react'
import { View, Image, Text } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { RectButton, TouchableOpacity } from 'react-native-gesture-handler'

import LandingImg from '../../assets/images/landing.svg'
import Icon from 'react-native-vector-icons/Ionicons'

import styles from './styles'
import {useAuth} from '../../context/Auth'

const TAG = 'Landing'
//Icon.loadFont();

/**
 * App home screen
 */
const Landing = () => {

    const navigation = useNavigation()
    const {getTotalConnections, user, signOut} = useAuth()
    const [total, setTotal] = useState(0)

    useEffect(() => {
        getTotalConnections().then((t:number) => {
            setTotal(t)
        })
    }, [])

    /**
     * Navigate to GiveClasses screen
     * called by GiveClasses button
     */
    function handleNavigateToGiveClasses() {
        navigation.navigate('GiveClasses')
    }

    /**
     * Navigate to Study screen
     * called by Study button
     */
    function handleNavigateToStudy() {
        navigation.navigate('Study')
    }

    function handleSignOut(){
        signOut()
    }

    function handleProfile() {
        navigation.navigate('Profile')
    }

    return (
        <View style={styles.container}>
            <View style={[styles.primaryContainer]}>
               <View style={[styles.header]}>
                    <TouchableOpacity onPress={handleProfile}>
                        <View style={[styles.headerAvatar]}>
                            {
                                user.avatar?<Image source={{uri: user.avatar}} style={styles.headerAvatarImg}/> : <Icon name='person-sharp' style={styles.headerAvatarSvg} />
                            }
                            <Text style={styles.headerAvatarTxt}>{user.name}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={handleSignOut}>
                    <View style={styles.headerSignOut} >
                        <Icon name='power-outline' style={styles.headerSignOutImg}/>
                    </View>
                    </TouchableOpacity>
                </View>
                <LandingImg />
            </View>
            <View style={[styles.secondaryContainer]}>
                <Text style={styles.title}>Seja Bem-vindo,{'\n'}
                    <Text style={styles.titleBold}>O que deseja fazer?</Text>
                </Text>

                <View style={styles.buttonContainer}>
                    <RectButton style={[styles.button, styles.buttonPrimary]} onPress={handleNavigateToStudy}>
                        <Icon style={styles.buttonImg} name='book-outline' size={40} />
                        <Text style={styles.buttonText}>Estudar</Text>
                    </RectButton>

                    <RectButton style={[styles.button, styles.buttonSecondary]} onPress={handleNavigateToGiveClasses}>
                        <Icon style={styles.buttonImg} name='tv-outline' size={40} />
                        <Text style={styles.buttonText}>Dar aulas</Text>
                    </RectButton>
                </View>

                <Text style={styles.totalConnections}>
                    Total de {total} conexões{'\n'} já realizadas {' '}
                    <Icon name="heart" style={styles.heart} />  
                </Text>
            </View>
        </View>
    )
}

export default Landing