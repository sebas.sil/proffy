import {StyleSheet} from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background,
        justifyContent: 'center'
    },

    secondaryContainer: {
        flex: 1,
        padding: padding.xl
    },

    primaryContainer: {
        flex: 1,
        padding: padding.xl,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primary
    },

    header: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        height: 40, 
        position: 'absolute', 
        top: 20, 
        width: '100%', 
        alignItems: 'center', 
        left: 40
    },

    headerAvatar: {
        flexDirection: 'row', 
        alignItems: 'center'
    },

    headerAvatarTxt: {
        fontSize: 20,
        fontFamily: fonts.primary,
        color: colors.colorTextPrimaryLight,
        paddingLeft: padding.sm
    },

    headerSignOut: {
        backgroundColor: colors.primaryDark, 
        borderRadius: 8, 
        height: 40, 
        width: 40, 
        alignItems: 'center', 
        justifyContent: 'center'
    },

    headerSignOutImg: {
        fontSize: 20,
        color: colors.colorTextPrimaryLight
    },

    headerAvatarImg: {
        borderRadius: 20,
        height: 40,
        width: 40,
    },

    headerAvatarSvg: {
        fontSize: 40,
        color: colors.primaryDark
    },

    title: {
        fontFamily: fonts.primary,
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.sm * 3,
        marginVertical: padding.lg
    },

    titleBold: {
        fontFamily: fonts.primaryBold,
    },


    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },

    button: {
        height: 158,
        width: 147,
        backgroundColor: '#333',
        borderRadius: 8,
        padding: 24,
        justifyContent: 'space-between'
    },

    buttonPrimary: {
        backgroundColor: colors.primary
    },

    buttonSecondary: {
        backgroundColor: colors.secondary
    },

    buttonText: {
        fontFamily: fonts.secondaryBold,
        color: 'white',
        fontSize: fonts.sm * 3
    },

    buttonImg: {
        color: 'white'
    },

    totalConnections: {
        fontFamily: fonts.primary,
        color: colors.placeholder,
        fontSize: fonts.md,
        marginTop: 40,
        position: 'relative'
    },

    heart: {
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.md
    },
})

export default styles