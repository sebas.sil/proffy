import React, {useState, useEffect} from 'react'
import {View, ImageBackground, Text} from 'react-native'
import {IClass, ISchedule} from '../../services/api'
import {BorderlessButton, RectButton, ScrollView, TextInput, TouchableOpacity} from 'react-native-gesture-handler'
import PageHeader from '../../components/PageHeader'
import Input from '../../components/Input'
import Dropdown from '../../components/Dropdown'
import {useAuth} from '../../context/Auth'
import Icon from 'react-native-vector-icons/Ionicons'

import styles from './styles'
import stylesG from '../style_global'
/**
 * Screen to create a new class, teacher and schedules its not implemented yet and just have an advice to do it throght website
 */
const GiveClasses = () => {

    const {user, update} = useAuth()

    const [schedule, setSchedule] = useState<ISchedule[]>([])
    const [subject, setSubject] = useState<string>('')
    const [cost, setCost] = useState<number>(0)
    const [valid, setValid] = useState(false)

    useEffect(() => {
        //TODO validate form
        let isValid = schedule?.length > 0
        setValid(isValid)
    }, [schedule, subject, cost])

    useEffect(() => {
        if(user.class?.cost){
            setCost(user.class?.cost)
        }
        if(user.class?.subject){
            setSubject(user.class?.subject)
        } else {
            setSubject('')
        }
        if(user.class?.schedule){
            setSchedule(user.class?.schedule)
        }
    }, [])
    /**
     * Add a new empty schedule item to the array, called by 'add new item' button
     */
    function addSchedule() {
        setSchedule([...schedule, {id: schedule.length} as ISchedule])
    }

    /**
     * Update the value of the property name of a specific schedule item
     * Called by onChange of the inputs
     * @param key , number - schedule item unique ID
     * @param field , string - property name to seek
     * @param value , string - property value to be updated
     */
    function setSelecteditem(key: number, field: string, value: string) {
        const arr = schedule.map((s: ISchedule) => {
            if (s.id === key) {
                return {...s, [field]: value}
            }

            return s
        })
        setSchedule(arr)
    }

    /**
     * Post the request to create a class, teacher and schedule items.
     * Called by submit button
     */
    function handleSubmit() {
        update({id: user.class?.id || 0, subject, cost, schedule})
    }

    const timeFormating = (text: string): string => {
        // remove o que nao for numerico
        text = text.replace(/[^0-9]/g, '')
        //remoge os zeros
        text = String(Number(text))
        if (text.length < 5) {
            // adiciona zeros para a mascara
            text = text.padStart(4, '0')
            // formata
            let part1 = text.slice(0, 2)
            let part2 = text.slice(2, 4)

            text = part1 + ':' + part2
        } else {
            text = String(Number(text)).slice(0, 4)
            text = text.slice(0, 2) + ':' + text.slice(2, 4)
        }
        return text
    }

    return (
        <View style={styles.container}>
            <PageHeader title='Dar Aulas' />
            <ScrollView>
                <View style={styles.primaryContainer}>
                    <Text style={styles.headerAvatarName}>Que incrível que você quer dar aulas.</Text>
                    <Text style={styles.headerAvatarSubject}>O primeiro passo, é preencher esse formulário de inscrição.</Text>
                </View>
                <View style={styles.secondaryContainer}>
                    
                    <View style={styles.secondaryForm}>
                        <View style={styles.secondaryFormLegendContainer}>
                            <Text style={styles.secondaryFormLegend}>Sobre a aulas</Text>
                            <BorderlessButton onPress={addSchedule}><Text style={[styles.secondaryFormAdd]}>+ Novo</Text></BorderlessButton>
                        </View>

                        <Dropdown label='Matéria' style={styles.input} items={[
                            {value: 'artes', label: 'Artes'},
                            {value: 'biologia', label: 'Biologia'},
                            {value: 'portugues', label: 'Português'},
                            {value: 'matematica', label: 'Matemática'}
                        ]} selectedValue={subject} onValueChange={setSubject} />
                        <Input label='Custo' value={String(cost)} style={styles.input} keyboardType='decimal-pad' textContentType='name' onChangeText={setCost} />
                        {schedule && schedule.map((s: ISchedule, i: number) => {
                            return (
                                <View key={i}>
                                    <Dropdown label='Dia da semana' style={styles.input} items={[
                                        {value: '0', label: 'Domingo'},
                                        {value: '1', label: 'Segunda-feira'},
                                        {value: '2', label: 'Terça-feira'},
                                        {value: '3', label: 'Quarta-feira'},
                                        {value: '4', label: 'Quinta-feira'},
                                        {value: '5', label: 'Sexta-feira'},
                                        {value: '6', label: 'Sábado'}
                                    ]} selectedValue={String(s.week_day)} onValueChange={(i: string) => setSelecteditem(s.id, 'week_day', i)} />
                                    <View style={styles.inputTimeContainer}>
                                        <Input label='Das' value={s.from} style={[styles.input, {flex: 1}]} keyboardType='numeric' textContentType='none' onChangeText={(t: string) => setSelecteditem(s.id, 'from', timeFormating(t))} />
                                        <Input label='Até' value={s.to} style={[styles.input, {flex: 1}]} keyboardType='numeric' textContentType='none' onChangeText={(t: string) => setSelecteditem(s.id, 'to', timeFormating(t))} />
                                    </View>
                                </View>
                            )
                        })}
                        
                    </View>
                    <View style={styles.footer}>
                            <RectButton>
                                <TouchableOpacity style={[styles.loginButton, !valid && stylesG.disabled]} onPress={handleSubmit}>
                                    <Text style={styles.loginButtonText}>Salvar cadastro</Text>
                                </TouchableOpacity>
                                <View style={styles.warningConteiner}>
                                    <Icon name='warning-outline' alt="Aviso importante" style={styles.warnIcon} />

                                    <View style={styles.warningTxtConteiner}>
                                        <Text style={[styles.warningTxt, styles.warningTxtTitle]}>Importante!</Text>
                                        <Text style={styles.warningTxt}>Preencha todos os dados</Text>
                                    </View>
                                </View>
                            </RectButton>
                        </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default GiveClasses