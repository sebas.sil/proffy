import { StyleSheet } from 'react-native'
import {colors, dimensions, fonts, padding} from '../style_global'

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    primaryContainer: {
        minHeight: (dimensions.fullHeight / 2) - padding.xl,
        backgroundColor: colors.primary,
        padding: padding.xl
    },

    secondaryContainer: {
        alignItems: 'center'
    },

    
    headerAvatarName:{
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.sm * 5,
        marginTop: padding.md,
        color: colors.colorTextPrimaryLight
    },

    headerAvatarSubject: {
        fontFamily: fonts.primary,
        fontSize: fonts.sm * 3,
        color: colors.colorTextPrimaryLight,
        paddingTop: padding.lg
    },

    secondaryForm: {
        width: '90%',
        backgroundColor: 'white',
        marginTop: -padding.xl,
        borderTopStartRadius: 8,
        borderTopEndRadius: 8,
        padding: padding.lg,
        borderBottomColor: colors.backgroundDark,
        borderBottomWidth: 1,
    },

    secondaryFormLegendContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: padding.md,
        borderBottomWidth: 1,
        paddingBottom: padding.md,
        borderBottomColor: colors.background
    },

    secondaryFormLegend: {
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.lg
    },

    secondaryFormAdd: {
        color: colors.primary
    },

    input: {
        marginBottom: padding.md,
        backgroundColor: colors.backgroundLight
    },

    inputTimeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },

    loginButton: {
        backgroundColor: '#04d361',
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8
    },

    loginButtonText: {
        color: 'white',
        fontSize: fonts.md,
        fontFamily: 'Archivo-Bold'
    },

    footer: {
        padding: padding.lg,
        width: '90%',
        backgroundColor: colors.backgroundLight,
        borderBottomStartRadius: 8,
        borderBottomEndRadius: 8,
        marginBottom: padding.md
    },

    warningConteiner: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: padding.md,

    },

    warnIcon: {
        color: colors.primary,
        fontSize: fonts.md * 3,
        marginRight: padding.sm
    },

    warningTxtTitle: {
        color: colors.primary
    },

    warningTxt: {
        fontFamily: fonts.primary,
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.md
    }

})

export default styles