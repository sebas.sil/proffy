import React, {useState, useEffect} from 'react'
import {View, Text, KeyboardAvoidingView, TouchableOpacity} from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import Background from '../../assets/images/background-primary-1.svg'
import Logo from '../../assets/images/logo.svg'
import Input from '../../components/Input'
import styles from './styles'
import {useAuth} from '../../context/Auth'
import Icon from 'react-native-vector-icons/Ionicons'
import {RectButton} from 'react-native-gesture-handler'
import {useNavigation, CommonActions} from '@react-navigation/native'
import {BorderlessButton} from 'react-native-gesture-handler'
import stylesG from '../style_global'



/**
 * App home screen
 */
const Login = () => {

    const navigation = useNavigation()
    const {signIn} = useAuth()

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [remember, setRememeber] = useState<boolean>(false)
    const [showPassword, setShowPassword] = useState<boolean>(false)
    const [valid, setValid] = useState<boolean>(false)

    useEffect(() => {
        // clear navigation stack
        navigation.dispatch(state => {
            // Remove all but login route from the stack
            const routes = state.routes.filter(r => r.name === 'Login');
          
            return CommonActions.reset({
              ...state,
              routes,
              index: routes.length - 1,
            })
          })
    }, [])

    useEffect(() => {
        setValid(email.length > 0 && password.length > 0)
    }, [email, password])

    function toggleEye() {
        setShowPassword(!showPassword)
    }

    function handleLogin() {
        signIn(email, password, remember)
    }

    function handleRegister() {
        navigation.navigate('Register')
    }

    function handleForgot() {
        navigation.navigate('Forgot')
    }

    return (
        <>

            <View style={[styles.container]}>
                <View style={[styles.primaryContainer]}>
                    <View style={{width: 170}}>
                        <Logo style={[styles.logo, stylesG.textPrimaryLight]} width={160} height={46} />
                        <Text style={[stylesG.textPrimaryLight]}>
                            Sua plataforma de estudos online.
                        </Text>
                    </View>
                    <Background style={styles.bg} />
                </View>
                <KeyboardAvoidingView style={[styles.secondaryContainer]} behavior='padding'>
                    <View style={[styles.secondary]}>
                        <View style={styles.loginFormTitle}>
                            <Text style={[styles.secondaryTitle]}>Fazer Login</Text>
                            <BorderlessButton onPress={handleRegister}><Text style={[styles.secondaryAccount]}>Criar uma conta</Text></BorderlessButton>
                        </View>
                        <Input value={email} placeholder='E-mail' onChangeText={setEmail} keyboardType='email-address' textContentType='username'/>
                        <Input value={password} placeholder='Senha' onChangeText={setPassword} secureTextEntry={!showPassword} textContentType='password'>
                            <Icon name={showPassword ? 'eye-off-outline' : 'eye-outline'} style={[styles.eye]} onPress={toggleEye} />
                        </Input>
                        <View style={styles.pageLoginExtends}>
                            <View style={styles.check}>
                                <CheckBox name='rememberme' value={remember} onValueChange={setRememeber} />
                                <Text style={styles.checkText}>Lembre-me</Text>
                            </View>
                            <BorderlessButton onPress={handleForgot}><Text style={styles.pageLoginExtendsText}>esqueci minha senha</Text></BorderlessButton>
                        </View>
                        <RectButton>
                            <TouchableOpacity style={[styles.loginButton, !valid && stylesG.disabled]} onPress={handleLogin} disabled={!valid}>
                                <Text style={styles.loginButtonText}>Entrar</Text>
                            </TouchableOpacity>
                        </RectButton>
                    </View>
                </KeyboardAvoidingView>
            </View>
        </>
    )
}

export default Login