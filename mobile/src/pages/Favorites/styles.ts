import { StyleSheet, Platform, StatusBar } from "react-native";
import {colors, dimensions, fonts, padding} from "../style_global";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    primaryContainer: {
        backgroundColor: colors.primary,
        padding: padding.xl,
        minHeight: (dimensions.fullHeight - (padding.xl * 2) - STATUSBAR_HEIGHT) /2
    },

    primaryTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    headerTitle: {
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.lg,
        marginTop: padding.md,
        color: colors.colorTextPrimaryLight,
        width: '50%',
    },

    secondaryContainer: {
        flex: 1,
        marginHorizontal: 10
    },

    favoriteList: {
        marginTop: -40
    },

    headerCountContainer: {
        flexDirection: 'row', 
        alignItems: 'center'
    },

    headerCountTxt: {
        marginLeft: padding.sm, 
        fontSize: fonts.md, 
        color: colors.colorTextPrimaryLight
    },
    
    headerTitleIcon: {
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.sm * 3,

    },
    
})

export default styles