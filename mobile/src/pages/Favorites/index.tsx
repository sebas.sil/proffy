import React, {useState, useEffect} from 'react'
import {View, ScrollView, Text} from 'react-native'

import styles from './styles'
import PageHeader from '../../components/PageHeader'
import TeacherItem from '../../components/TeacherItem'
import {IUser} from '../../services/api'
import {useFavorites} from '../../context/Favorites'
import Icon from 'react-native-vector-icons/Ionicons'

/**
 * Screen to show the classes maked as favorites (its saved locally)
 */
const Favorites = () => {

    const {favorites, removeFromFavorites, addToFavorites} = useFavorites();

    function toggleFavorite(item: IUser) {
        console.log('toggleFavorite', item.id)
        removeFromFavorites(item).catch(console.log)
    }

    return (
        <View style={styles.container}>
            <PageHeader title='Favoritos' />
            <ScrollView>
                <View style={styles.primaryContainer}>
                    <View style={styles.primaryTitleContainer}>
                        <Text style={styles.headerTitle}>Meus Proffys favoritos.</Text>
                        <View style={styles.headerCountContainer}>
                            <Icon name='heart-outline' style={styles.headerTitleIcon} />
                            <Text style={styles.headerCountTxt}>{favorites.length}</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.secondaryContainer, styles.favoriteList]}>
                    {favorites.map((u: IUser) => {
                        return <TeacherItem key={u.id} item={u} favorited={true} haddleToggleFavorite={() => toggleFavorite(u)} />
                    })}
                </View>
            </ScrollView>
        </View>
    )
}

export default Favorites