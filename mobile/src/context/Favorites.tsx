import React, { createContext, useState, useContext, useEffect } from 'react'
import {IUser} from '../services/api'
import {useMessages} from './Message'
import AsyncStorageStatic from '@react-native-community/async-storage'

const TAG = 'FAVORITES_CONTEXT'
interface FavoritesData {
    addToFavorites(user:IUser):Promise<void>
    removeFromFavorites(user:IUser):Promise<void>
    favorites:Array<IUser>
}

const FavoritesContext = createContext<FavoritesData>({} as FavoritesData)

const FavoritesProvider: React.FC = ({ children }) => {

    const [favorites, setFavorites] = useState<Array<IUser>>([])
    const {setText} = useMessages()

    useEffect(()=>{
        AsyncStorageStatic.getItem('favorites').then( items => {
            if(items){
                setFavorites(JSON.parse(items))
            }
        })
    }, [])

    useEffect(()=>{
        AsyncStorageStatic.setItem('favorites', JSON.stringify(favorites)).catch(console.log)
    }, [favorites])

    async function addToFavorites(user:IUser):Promise<void> {
        const found = favorites.findIndex((e:IUser) => e.id === user.id) != -1
        if(!found){
            setFavorites([...favorites, user])
            setText('Item adicionado aos favoritos')
        } else {
            setText('Item já está nos favoritos')
        }
    }
    async function removeFromFavorites(user:IUser):Promise<void> {
        const idx = favorites.findIndex((u:IUser) => u.id === user.id)
        if(idx != -1){
            favorites.splice(idx, 1)
            setFavorites([...favorites])
            setText('Item removido dos favoritos')
        } else {
            setText('Item não encontrado nos favoritos')
        }
    }

    return (
        <FavoritesContext.Provider value={{ addToFavorites, removeFromFavorites, favorites }}>
            {children}
        </FavoritesContext.Provider>
    )
}

const useFavorites = () => {
    return useContext(FavoritesContext)
}

export { FavoritesProvider, useFavorites }