import React, {createContext, useState, useEffect, useContext} from 'react'
import * as auth from '../services/api'
import {useMessages, ErrorType} from './Message'
import AsyncSorage from '@react-native-community/async-storage'
const TAG = 'AUTH'
/**
 *
 */
interface AuthContextData {
    signed: boolean
    user: auth.IUser
    signIn(email: string, password: string, remember?: boolean): Promise<void>
    signOut(): Promise<void>
    register(name: string, email: string, password: string, remember?: boolean): Promise<void>
    reset(email: string, token: string, password: string, remember?: boolean): Promise<void>
    forgot(email: string): Promise<void>
    getTotalConnections(): Promise<number>
    update(user: auth.IClass): Promise<auth.IClass>
    update(user: auth.IUser): Promise<auth.IUser>
    //loading: boolean
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData)

const AuthProvider: React.FC = ({children}) => {

    const [user, setUser] = useState<auth.IUser>({} as auth.IUser)
    const [token, setToken] = useState<string>('')
    const [remember, setRemember] = useState<boolean>(false)
    //const [loading, setLoading] = useState(true)
    const {setText, setType} = useMessages()


    useEffect(() => {
        //setLoading(true)
        AsyncSorage.multiGet(['@Proffy:user', '@Proffy:token']).then(d => {
            if (d) {
                if (d[0][1]) {
                    setUser(JSON.parse(d[0][1]))
                }

                if (d[1][1]) {
                    setToken(d[1][1])
                }
            }
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao carregar valores do banco')
            console.error(e)
        })//.finally(() => setLoading(false))
    }, [])


    useEffect(() => {
        if (remember) {
            AsyncSorage.setItem('@Proffy:token', JSON.stringify(token)).catch((e) => {
                setType(ErrorType.ERROR)
                setText('Erro ao gravar token no banco')
                console.error(e)
            })//.finally(() => setLoading(false))
        }
        auth.setAuthToken(token)
    }, [token])

    useEffect(() => {
        if (remember) {
            AsyncSorage.setItem('@Proffy:user', JSON.stringify(user)).catch((e) => {
                setType(ErrorType.ERROR)
                setText('Erro ao gravar usuario no banco')
                console.error(e)
            })//.finally(() => setLoading(false))
        }
    }, [user])

    async function signIn(email: string, password: string, remember = false) {
        console.log(TAG, 'sign in', email)
        setRemember(remember)
        //setLoading(true)
        return auth.signIn(email, password).then((res) => {
            const {user, token} = res
            setToken(token)
            setUser(user)
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao realizar login, verifique suas credenciais')
            console.error(e)
        })//.finally(() => setLoading(false))//.finally(() => setLoading(false))
    }

    async function signOut() {
        console.log(TAG, 'sign out')
        //setLoading(true)
        AsyncSorage.multiRemove(['@Proffy:user', '@Proffy:token'])
        setUser({} as auth.IUser)
        setToken('')
        setType(ErrorType.SUCCESS)
        setText('Esperamos vê-lo em breve')
        //setLoading(false)
    }

    async function register(name: string, email: string, password: string, remember = false) {
        console.log(TAG, 'register', email)
        setRemember(remember)
        //setLoading(true)
        auth.register(name, email, password).then((res) => {
            const {user, token} = res
            //console.log(TAG, 'register', user, token)
            setType(ErrorType.SUCCESS)
            setText('Agora você faz parte da plataforma da Proffy. Tenha uma ótima experiência.')
            setToken(token)
            setUser(user)
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao registrar usuário')
            console.error(e)
        })//.finally(() => setLoading(false))//.finally(() => setLoading(false))
    }

    async function reset(email: string, password: string, token: string, remember = false): Promise<void> {
        console.log(TAG, 'reset', email)
        setRemember(remember)
        //setLoading(true)
        auth.resetPassword(email, password, token).then((res) => {
            const {user, token} = res
            console.log(TAG, 'register', user, token)
            setUser(user)
            setToken(token)
            setType(ErrorType.SUCCESS)
            setText('Boa, agora é só aproveitar os estudos.')
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao redefinir senha')
            console.error(e)
        })//.finally(() => setLoading(false))//.finally(() => setLoading(false))
    }

    async function forgot(email: string): Promise<void> {
        console.log(TAG, 'forgot', email)
        //setLoading(true)
        auth.forgotPassword(email).then((res) => {
            setType(ErrorType.SUCCESS)
            setText('Boa, agora é só checar o e-mail que foi enviado para você redefinir sua senha e aproveitar os estudos.')
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao carregar enviar e-mail de redefinição de senha')
            console.error(e)
        })//.finally(() => setLoading(false))//.finally(() => setLoading(false))
    }

    async function getTotalConnections(): Promise<number> {
        console.log(TAG, 'total connections')
        //setLoading(true)
        return auth.getTotalConnections().then((res) => {
            const {total} = res
            return total
        }).catch(e => {
            console.error(TAG, 'total connections', e)
            setType(ErrorType.ERROR)
            setText('Erro ao carregar numero de conexões já realizadas')
            console.error(e)
            return 0
        })//.finally(() => setLoading(false))
    }

    async function update(user: auth.IUser): Promise<auth.IUser>
    async function update(user: auth.IClass): Promise<auth.IClass>

    async function update(input: any): Promise<any> {
        console.log(TAG, 'update', 'cost' in input ? 'class' : 'user')
        const txt = 'cost' in input ? 'classe' : 'usuario'
        //setLoading(true)
        return auth.update(input).then((res) => {
            setType(ErrorType.SUCCESS)
            setText(txt + ' atualizado')
            if ('cost' in input) {
                user.class = input
                setUser(user)
            } else {
                setUser(input)
            }
            return res
        }).catch((e) => {
            setType(ErrorType.ERROR)
            setText('Erro ao atualizar' + txt)
            console.error(e)
        })//.finally(() => setLoading(false))//.finally(() => setLoading(false))
    }

    return (
        <AuthContext.Provider value={{signed: Object.keys(user).length > 0, user, signIn, signOut, register, reset, forgot, getTotalConnections, update}}>
            {children}
        </AuthContext.Provider>
    )
}

const useAuth = () => {
    return useContext(AuthContext)
}

export {AuthProvider, useAuth}