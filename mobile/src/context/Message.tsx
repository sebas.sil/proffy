import React, { createContext, useState, useContext } from 'react'

enum ErrorType {
    SUCCESS = 'success',
    ERROR = 'error'
}

interface MessagesData {
    text: string,
    type: ErrorType,
    setText(txts: string): void
}

const MessagesContext = createContext<MessagesData>({} as MessagesData)

const MessagesProvider: React.FC = ({ children }) => {

    const [text, setText] = useState<Array<string>>()
    const [type, setType] = useState<Array<string>>()

    return (
        <MessagesContext.Provider value={{ text, setText, type, setType }}>
            {children}
        </MessagesContext.Provider>
    )
}

const useMessages = () => {
    return useContext(MessagesContext)
}

export { MessagesProvider, useMessages, ErrorType }