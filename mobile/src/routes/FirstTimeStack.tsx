import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Splash from '../pages/Splash'
import Step1 from '../pages/Splash/Step1'
import Step2 from '../pages/Splash/Step2'
import Login from '../pages/Login'
const { Navigator, Screen } = createStackNavigator()

/**
 * Create the routes between the screens
 * This configuration does a stack (LIFO)
 */
const AppStack = () => {
    return (
        <Navigator screenOptions={{ headerShown: false }}>
            <Screen name='Splash' component={Splash} />
            <Screen name='Step1' component={Step1} />
            <Screen name='Step2' component={Step2} />
            <Screen name='Login' component={Login} />
        </Navigator>
    )
}

export default AppStack