import React from 'react'
import Alert from '../components/Alert'
import {useAuth} from '../context/Auth'
import Authenticated from './Authenticated.routes'
import Unauthenticated from './Unauthenticated.routes'

const Routes = () => {

    const { signed } = useAuth()
    
    return (
        <>
        {signed ? <Authenticated /> : <Unauthenticated />}
        <Alert />
        </>
    )
}

export default Routes