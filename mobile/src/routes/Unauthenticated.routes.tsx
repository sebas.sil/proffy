import React, {useEffect, useState} from 'react'
import {View, ActivityIndicator} from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import AsyncStorageStatic from '@react-native-community/async-storage'

import Login from '../pages/Login'
import FirstTimeStack from './FirstTimeStack'
import styles from '../pages/style_global'
import Register from '../pages/Register'
import Forgot from '../pages/Forgot'
const { Navigator, Screen } = createStackNavigator()

/**
 * Create the routes between the screens
 * This configuration does a stack (LIFO)
 */
const AppStack = () => {

    const [first, setFirst] = useState(false)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        AsyncStorageStatic.getItem('first_time').then(i => {
            setFirst(i == undefined)
            setLoading(false)
        }).catch(console.log)
    }, [])

    if(loading){
        return (
            <View style={styles.loading}>
                <ActivityIndicator size='large' color='#999'/>
            </View>
        )
    }

    return (
        <>
            <NavigationContainer>
                <Navigator screenOptions={{ headerShown: false }}>
                    {first ? 
                        <Screen name='Splash' component={FirstTimeStack} />
                    :
                        <Screen name='Login' component={Login} />
                    }
                    <Screen name='Register' component={Register} />
                    <Screen name='Forgot' component={Forgot} />
                </Navigator>
            </NavigationContainer>
        </>
    )
}

export default AppStack