import React from 'react'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import TeacherList from '../pages/TeacherList'
import Favorites from '../pages/Favorityes'
import Icon from 'react-native-vector-icons/Ionicons'
import {colors, fonts, padding} from '../pages/style_global'
import {FavoritesProvider} from '../context/Favorites'

const {Navigator, Screen} = createBottomTabNavigator()

/**
 * Create the routes between the screens
 * This configuration does tabs
 */
const StudyTabs = () => {
    return (
        <FavoritesProvider>
            <Navigator tabBarOptions={{
                style: {
                    elevation: 0,
                    shadowOpacity: 0,
                    height: 64
                },
                tabStyle: {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                },

                iconStyle: {
                    flex: 0,
                    width: 20,
                    height: 20
                },

                labelStyle: {
                    fontFamily: fonts.secondaryBold,
                    fontSize: fonts.sm * 1.5,
                    marginLeft: padding.sm * 1.5
                },

                inactiveBackgroundColor: colors.backgroundLight,
                activeBackgroundColor: colors.primaryLight,
                inactiveTintColor: colors.disabled,
                activeTintColor: colors.colorTextPrimaryDark
            }}>
                <Screen name="TeacherList" component={TeacherList} options={{
                    tabBarLabel: 'Proffys',
                    tabBarIcon: ({color, size, focused}) => {
                        return (
                            <Icon name='tv-outline' size={size} color={focused ? '#8257e5' : color} />
                        )
                    }
                }} />
                <Screen name="Favorites" component={Favorites} options={{
                    tabBarLabel: 'Favoritos',
                    tabBarIcon: ({color, size, focused}) => {
                        return (
                            <Icon name='heart-half-outline' size={size} color={focused ? '#8257e5' : color} />
                        )
                    }
                }} />
            </Navigator>
        </FavoritesProvider>
    )
}

export default StudyTabs