import React, {useState, useEffect} from 'react'
import {Modal, View, Text} from 'react-native'
import {useMessages} from '../../context/Message';

import styles from './styles'

/**
 * Show messages to the end user
 */
const Alert = () => {

    const [modalVisible, setModalVisible] = useState(false);
    const {text, setText} = useMessages()

    useEffect(() => {
        if(text) {
            setModalVisible(true)
            setTimeout(() => {
                setModalVisible(false)
                setText('')
            }, 2000)
        }
    }, [text])

    return (
        <Modal animationType="slide" transparent={true} visible={modalVisible}>
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Text style={styles.modalText}>{text}</Text>
                </View>
            </View>
        </Modal>
    )
}

export default Alert