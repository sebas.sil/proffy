import {StyleSheet} from "react-native";
import {colors, fonts, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    centeredView: {
        position: 'absolute',
        bottom: padding.md,
        width: '100%',
      },

      modalView: {
        backgroundColor: colors.primaryLight,
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        margin: padding.md,
        width: '90%',
        padding: padding.lg,
        shadowColor: "black",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4
      },
      modalText: {
        fontFamily: fonts.primary
      },
      modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontFamily: fonts.primaryBold
      }
})

export default styles