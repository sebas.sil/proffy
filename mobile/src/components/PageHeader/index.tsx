import React, { ReactNode } from 'react'
import { View, Image, Text } from 'react-native'

import { BorderlessButton } from 'react-native-gesture-handler'

import Icon from 'react-native-vector-icons/Ionicons'
import LogoImg from '../../assets/images/logo.svg'
import { useNavigation } from '@react-navigation/native'
import StatusBar from '../StatusBar'

import styles from './styles'
import {colors} from '../../pages/style_global'
/**
 * Interface to force parameters use and type
 */
interface PageHeaderProps {
    /**
     * Title to be shown to the user when this component is rendered
     */
    title: string
}

/**
 * Header component to be shown in all app screens
 * @param title, string - required - Title of the header page
 * @param headerRight, react component - shown at rigth side of the header
 * @param children, React components - optional - Children elements to be added to the header
 */
const PageHeader: React.FC<PageHeaderProps> = ({ title }) => {

    const navigation = useNavigation()

    /**
     * back button to back to home screen
     */
    function handleGoBack() {
        navigation.navigate('Landing')
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.primaryDark} />
            <View style={styles.topBar}>
                <View style={styles.backIconContainer}>
                    <BorderlessButton onPress={handleGoBack}>
                        <Icon style={styles.backIcon} name='arrow-back-outline' />
                    </BorderlessButton>
                </View>

                <View style={[styles.header]}>
                    <Text style={styles.title}>{title}</Text>
                </View>

                <View style={[styles.logoContainer]}>
                    <LogoImg style={styles.logo} width={50} height={30}/>
                </View>
            </View>
        </View>
    )
}

export default PageHeader