import { StyleSheet } from "react-native";
import {colors, fonts, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    container: {
        paddingVertical: padding.md,
        paddingHorizontal: padding.xl,
        backgroundColor: colors.primaryDark
    },

    topBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    backIconContainer: {
        flex: 1,
        width: '100%',
        height: '100%'
    },

    backIcon: {
        fontSize: 25,
        color: colors.colorTextPrimaryLight,
    },

    logo: {
        color: colors.colorTextPrimaryLight,
        alignSelf: 'flex-end'
    },

    logoContainer: {
        flex: 1
    },

    title: {
        fontFamily: fonts.primary,
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.md,
        width: '100%',
        textAlign: 'center'
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1
    }
})

export default styles