import React, {useState, useEffect} from 'react'
import {View, Text} from 'react-native'
import {BorderlessButton, RectButton, TextInput} from 'react-native-gesture-handler'
import Dropdown from '../Dropdown'
import Icon from 'react-native-vector-icons/Ionicons'

import styles from './styles'
import {colors} from '../../pages/style_global'

interface IFilter {
    submit(subject:string, weekDay:string, time:string):Promise<void>
    isVisible:boolean
}

const Filter:React.FC<IFilter> = ({submit, isVisible}) => {

    const [subject, setSubject] = useState<string>('0')
    const [weekDay, setWeekDay] = useState<string>('')
    const [time, setTime] = useState<string>()
    const [filterVisible, setFilterVisible] = useState(true)

    useEffect(() => {
        setFilterVisible(isVisible)
    }, [isVisible])
    /**
     * Show or hide the header filter
     * called by filter show/hide button
     */
    function toggleFilterVisibility() {
        setFilterVisible(!filterVisible)
    }

    return (
        <View style={styles.searchForm}>
            <BorderlessButton style={[styles.searchFormFilterContent]} onPress={toggleFilterVisibility}>
                <Icon name='funnel-outline' style={styles.searchFormIcon} />
                <Text style={styles.searchFormText}>Filtrar por dia, hora e matéria</Text>
                <Icon name={filterVisible ? 'chevron-down-outline' : 'chevron-up-outline'} style={[styles.searchFormToggleIcon, styles.searchFormIcon]} />
            </BorderlessButton>
            {filterVisible &&
                <>
                    <Text style={styles.label}>Matéria</Text>
                    <Dropdown style={styles.input} items={[
                        {value: 'artes', label: 'Artes'},
                        {value: 'biologia', label: 'Biologia'},
                        {value: 'portugues', label: 'Portugues'},
                        {value: 'matematica', label: 'Matematica'}
                    ]} selectedValue={subject} onValueChange={setSubject} />

                    <View style={styles.inputGroup}>
                        <View style={[{flex: 2}]}>
                            <Text style={styles.label}>Dia da Semana</Text>
                            <Dropdown style={styles.input} items={[
                                {value: '0', label: 'Domingo'},
                                {value: '1', label: 'Segunda-feira'},
                                {value: '2', label: 'Terça-feira'},
                                {value: '3', label: 'Quarta-feira'},
                                {value: '4', label: 'Quinta-feira'},
                                {value: '5', label: 'Sexta-feira'},
                                {value: '6', label: 'Sábado'}
                            ]} selectedValue={weekDay} onValueChange={setWeekDay} />
                        </View>
                        <View style={[styles.inputBlock, {flex: 1}]}>
                            <Text style={styles.label}>Horário</Text>
                            <TextInput value={time} style={styles.input} placeholder="Qual horário?" placeholderTextColor={colors.placeholder} keyboardType='numeric' textContentType='none' onChange={e => {
                                // pega o valor
                                let text = e.nativeEvent.text
                                // remove o que nao for numerico
                                text = text.replace(/[^0-9]/g, '')
                                //remoge os zeros
                                text = String(Number(text))
                                if (text.length < 5) {
                                    // adiciona zeros para a mascara
                                    text = text.padStart(4, '0')
                                    // formata
                                    text = text.slice(0, 2) + ':' + text.slice(2, 4)
                                    setTime(text)
                                }
                            }} />
                        </View>
                    </View>

                    <RectButton style={styles.submitButton} onPress={() => submit(subject, weekDay, time)}>
                        <Text style={styles.submitButtonText}>Filtar</Text>
                    </RectButton>
                </>
            }


        </View>
    )
}

export default Filter