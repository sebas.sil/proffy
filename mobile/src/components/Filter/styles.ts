import {StyleSheet} from "react-native";
import {colors, fonts, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    searchForm: {
        marginBottom: 24
    },

    label: {
        color: '#d4c2ff',
        fontFamily: 'Archivo_400Regular'
    },

    input: {
        height: 54,
        backgroundColor: '#fff',
        borderRadius: 8,
        justifyContent: 'center',
        paddingHorizontal: 16,
        marginTop: 4,
        marginBottom: 16
    },

    inputGroup: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    inputBlock: {
        marginLeft: 10
    },

    submitButton: {
        backgroundColor: '#04b361',
        height: 56,
        flexDirection: 'row',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },

    submitButtonText: {
        color: '#fff',
        fontFamily: 'Archivo_700Bold',
        fontSize: 16
    },

    searchFormFilterContent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: padding.md
    },

    searchFormIcon: {
        color: colors.colorTextPrimaryLight,
        fontSize: fonts.sm * 3
    },

    searchFormText: {
        fontFamily: fonts.secondary,
        color: colors.colorTextPrimaryLight,
        marginLeft: padding.md,
        flex: 1
    },

})

export default styles