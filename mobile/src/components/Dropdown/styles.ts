import { StyleSheet } from "react-native";
import {colors, fonts, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    
    input: {
        height: 54,
        backgroundColor: 'white',
        borderRadius: 8,
        justifyContent: 'center',
        paddingHorizontal: padding.lg / 2,
        borderColor: colors.backgroundDark,
        borderWidth: 1,
        width: '100%'
    }
})

export default styles