import React, { useState } from 'react'
import { ViewStyle, View, Text } from 'react-native'
import {Picker} from '@react-native-community/picker'

import styles from './styles'
import stylesG from '../../pages/style_global'
/**
 * Interface to force parameters use and type
 */
interface IDropdown extends Picker {
    /**
     * component values options
     * This options is used to show the dropdown values to the user and this component is rendered
     */
    items: Array<{
        /**
         * option label
         * This label is used to show to the user and this option is rendered
         */
        label: string,
        /**
         * option unique name
         * This name is used to send the value when submited
         */
        value: string
    }>,
    /**
     * StyleSheet to customize this component
     */
    style?: ViewStyle,
    label?: string
}
const Dropdown: React.FC<IDropdown> = ({ items, label, style, ...args }) => {

    return (
        <>
        {label && <Text style={stylesG.label}>{label}</Text>}
        <View style={[styles.input, style]}>
            <Picker  {...args}>
                <Picker.Item label='Selecione um valor' value='' />
                {items.map(e => {
                    return <Picker.Item key={e.value} label={e.label} value={e.value} />
                })}
            </Picker>
        </View>
        </>
    )
}

export default Dropdown