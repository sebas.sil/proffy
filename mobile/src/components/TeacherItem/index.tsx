import React, {useState, useEffect} from 'react'
import {View, Image, Text, Linking} from 'react-native'

import styles from './styles'
import {RectButton} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import IClass from '../interfaces/Iclass'
import api, {ISchedule, IUser} from '../../services/api'
import {useFavorites} from '../../context/Favorites'

const TAG = 'TEACHER_ITEM'
/**
 * Interface to force parameters use and type
 */
interface ITeacher {
    /**
     * class item
     */
    item: IClass,
    /**
     * if this class is favorite or not
     */
    favorited?: boolean,

    /**
     * toggle between the favorite item
     * @param user {IUser}, user item
     */
    haddleToggleFavorite(): Promise<void>
}

/**
 * Cart component to represent a class with teacher, price and schedules
 * @param item, IClass - required - class item to be shown to the user 
 */

const TeacherItem: React.FC<ITeacher> = ({item, favorited, haddleToggleFavorite}) => {

    /**
     * opens the whatsapp app with its deeplink
     * called by the whatsapp button
     */
    function handleWhatsapp() {
        Linking.openURL('whatsapp://send?phone=' + item.phone)
        api.post('connections', {
            class_id: item.id
        }).catch(console.log)
    }

    function toggleFavorite() {
        haddleToggleFavorite()
    }

    /**
     * convert number to string week day
     * @return {string} weenk day name
     */
    function toWeekDay(v:number):string {
        switch (v) {
            case 0:
                return 'Domingo'
            case 1:
                return 'Segunda'
            case 2:
                return 'Terça'
            case 3:
                return 'Quarta'
            case 4:
                return 'Quinta'
            case 5:
                return 'Sexta'
            case 6:
                return 'Sabado'
            default:
                return '?'
        }
    }


    return (
        <View style={styles.container}>
            <View style={styles.profile}>
                {
                    item.avatar ? <Image source={{uri: item.avatar}} style={styles.headerAvatarImg} /> : <Icon name='person-sharp' style={styles.headerAvatarSvg} />
                }
                <View style={styles.profileInfo}>
                    <Text style={styles.name}>{item.name}</Text>
                    <Text style={styles.subject}>{item.subject}</Text>
                </View>
            </View>

            <Text style={styles.bio}>{item.bio}</Text>

            <View style={styles.footer}>
                <Text style={styles.price}>
                    Preço/Hora {'   '}
                    <Text style={styles.priceValue}>R$ {item.class.cost}</Text>
                </Text>

                <View style={styles.scheduleContainer}>
                    <View style={styles.teacherItemScheduleHeader}>
                        <Text>Dia</Text>
                        <Text>Horário</Text>
                    </View>
                    {item.class?.schedule.map((s:ISchedule, i:number) => {
                        return (
                            <View key={i} style={styles.teacherItemSchedule}>
                                <Text style={styles.teacherItemWeekDay}>{toWeekDay(s.week_day)}</Text>
                                <Text style={{flex: 1, height: 1, borderTopWidth: 1, width: '100%'}} />
                                <Text style={styles.teacherItemFrom}>{s.from}</Text>
                                <Text style={styles.teacherItemTo}>{s.to}</Text>
                            </View>
                        )
                    })}
                </View>

                <View style={styles.buttonsContainer}>
                    <RectButton style={[styles.favoriteButton, favorited ? styles.favorited : {}]} onPress={toggleFavorite}>
                        <Icon name={favorited ? 'heart-dislike-outline' : 'heart-outline'} color='white' size={25} />
                    </RectButton>
                    <RectButton style={styles.contactButton} onPress={handleWhatsapp}>
                        <Icon name='logo-whatsapp' size={25} color='white' />
                        <Text style={styles.contactButtonText}>Entrar em contato</Text>
                    </RectButton>
                </View>
            </View>
        </View>
    )
}

export default TeacherItem