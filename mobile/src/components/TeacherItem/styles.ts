import { StyleSheet } from "react-native";
import {color} from "react-native-reanimated";
import {colors, fonts, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: colors.backgroundDark,
        borderRadius: 8,
        marginBottom: padding.lg / 2,
        overflow: 'hidden'
    },

    profile: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: padding.md
    },

    headerAvatarImg: {
        borderRadius: 32,
        width: 64,
        height: 64,
    },

    headerAvatarSvg: {
        fontSize: 64,
        color: colors.primaryLight
    },

    profileInfo: {
        marginLeft: 16
    },

    name: {
        fontFamily: fonts.secondaryBold,
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.sm * 3
    },

    subject: {
        fontFamily: fonts.primary,
        color: colors.colorTextPrimaryDark,
        fontSize: fonts.md,
        marginTop: 4
    },

    bio: {
        marginHorizontal: 24,
        fontFamily: 'Poppins_400Regular',
        fontSize: fonts.md,
        lineHeight: 24,
        color: colors.colorTextPrimaryDark,
    },

    footer: {
        margin: 24,
        alignItems: 'center',
        marginTop: 24
    },

    price: {
        fontFamily: fonts.primary,
        color: colors.primary,
        fontSize: fonts.md
    },

    priceValue: {
        fontFamily: fonts.secondaryBold,
        color: colors.primary,
        fontSize: 16
    },

    buttonsContainer: {
        flexDirection: 'row',
        marginTop: 16
    },

    favoriteButton: {
        backgroundColor: colors.primary,
        width: 56,
        height: 56,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8,
        marginLeft: 8
    },

    favorited: {
        backgroundColor: '#e33d3d'
    },

    contactButton: {
        backgroundColor: colors.secondary,
        flex: 1,
        height: 56,
        flexDirection: 'row',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8
    },

    contactButtonText: {
        color: 'white',
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.md,
        marginLeft: 16
    },

    scheduleContainer: {
        width: '100%'
    },

    teacherItemScheduleHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5,
        paddingHorizontal: padding.xl
    },

    teacherItemWeekDay: {
        justifyContent: 'flex-start',
        flex: 1,
        paddingLeft: padding.md,
        fontFamily: fonts.secondaryBold,
        fontSize: fonts.md
    },

    teacherItemFrom: {
        paddingHorizontal: padding.md
    },

    teacherItemTo: {
        paddingHorizontal: padding.md
    },

    teacherItemSchedule: {
        flexDirection: 'row',
        backgroundColor: colors.backgroundLight,
        marginVertical: padding.sm / 2,
        paddingVertical: padding.sm,
        borderColor: colors.backgroundDark,
        borderWidth: 1,
        borderRadius: 8,
        alignItems: 'center'
    },
})

export default styles