import React from 'react'
import {View, StatusBar as Bar} from 'react-native'

import styles from './styles'

const StatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, {backgroundColor}]}>
        <Bar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);
export default StatusBar