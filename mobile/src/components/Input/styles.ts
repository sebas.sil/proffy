import { StyleSheet } from "react-native";
import {colors, padding} from "../../pages/style_global";

const styles = StyleSheet.create({
    inputBlock: {
        maxWidth: '100%',
        minWidth: '48%'
    },

    input: {
        height: 54,
        backgroundColor: 'white',
        borderRadius: 8,
        justifyContent: 'center',
        paddingHorizontal: padding.lg / 2,
        //marginTop: 4,
        //marginBottom: 16,
        borderColor: colors.backgroundDark,
        borderWidth: 1,
        width: '100%'
    }
})

export default styles