import React from 'react'
import {View, Text, TextInput} from 'react-native'
import {colors} from '../../pages/style_global'
import styles from './styles'
import stylesG from '../../pages/style_global'

interface IInputProps extends TextInput {
    placeholder: string,
    label?: string
}
const Input:React.FC<IInputProps> = ({placeholder, children, style, label, ...args}) => {
    return (
        <View style={styles.inputBlock}>
            {label && <Text style={stylesG.label}>{label}</Text>}
            <TextInput autoCapitalize ='none' style={[styles.input, style]} placeholder={placeholder} placeholderTextColor={colors.placeholder} {...args}/>
            {children}
        </View>
    )
}

export default Input