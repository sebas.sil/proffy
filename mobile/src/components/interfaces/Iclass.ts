/**
 * Class interface with all fields that a class has
 */
interface IClass {
    /**
     * Class unique indentification
     */
    id: number,
    /**
     * Theacher name
     */
    name: string,
    /**
     * Teacher avatar URL
     */
    avatar: string,
    /**
     * Class subject
     */
    subject: string,
    /**
     * Teacher and class biografy
     */
    bio: string,
    /**
     * Teacher price by hour
     */
    cost: number
    /**
     * Teacher contact phone 
     */
    phone: string
}

export default IClass