import React from 'react';
import AppStack from './src/routes'
import {StatusBar} from 'react-native'
import { AuthProvider } from './src/context/Auth'
import { MessagesProvider } from './src/context/Message'
import {colors} from './src/pages/style_global';

export default function App() {
  return (
    <>
      <MessagesProvider>
        <AuthProvider>
          <StatusBar backgroundColor={colors.primary} barStyle="light-content" />
          <AppStack />
        </AuthProvider>
      </MessagesProvider>
    </>
  );
}