# Proffy mobile

This project create the frontend (mobile) of Proffy system. This is responsable for send the RESTFull requests and rendering the native screens.

### Get Started

1. Intall `Yarn`: https://yarnpkg.com/getting-started/install
2. After cloning this repository run `yarn install` to intall all the dependencies

### Deploy / Run

To execute this frontend, go to the mobile root folder and run `yarn start`. This will run the react-native lib system. You need to connect your phone.

### Built With

[Visual Code 1.48.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [axios](https://www.npmjs.com/package/axios): HTTP client
- [react](https://www.npmjs.com/package/react): JavaScript library for creating user interfaces.
- [react-native](https://www.npmjs.com/package/react-native): React's declarative UI framework to iOS and Android
- [Proffy backend](../server/README.md): Proffy backend server 

### directory layout structure

    .
    ├── src                                 # Has all appl components and codes
    │   ├── assets                          # app resources folder
    │   │   └── images                      # images folder
    │   │       └── icons                   # icons folder
    │   ├── components                      # shared components. A component is 'shared' whe it is used in more than one page
    │   │   ├── Alert                       # reder the modal with messages
    │   │   ├── Dropdown                    # represent an compobox element
    │   │   ├── Filter                      # render the filter component on teachers search form
    │   │   ├── Input                       # render in input text on the forms
    │   │   ├── interfaces                  # interfaces used in more and one place
    │   │   ├── PageHeader                  # complet element representing an entire header with own properties
    │   │   ├── StatusBar                   # render the status bar to fill the top area with theme color
    │   │   └── TeacherItem                 # complex element representing an entire class card to show its atributes
    │   ├── context                         # context componentes to keep information up to date between screens
    │   ├── pages                           # nagevation screens of the app
    │   │   ├── Favorityes                  # screen to show all classes that was marked as favorites
    │   │   ├── Forgot                      # screen to recover password
    │   │   ├── GiveClasses                 # screen to register the class that the loggedin theacher gives
    │   │   ├── Landing                     # screen to show the Home page
    │   │   ├── Login                       # screen to authenticate the current user
    │   │   ├── Profile                     # screen to update the loggedin informations
    │   │   ├── Register                    # screen to register a new user
    │   │   ├── Splash                      # screen to show the welcome pages on the first usege
    │   │   └── TeacherList                 # screen to show the list and search classes screen
    │   ├── routes                          # configure the routes between the screens when user clicks on a navigation button
    │   └── services                        # HTTP client api ancapsulation
    └── ...                                 # others app files to configure this repository and project

### Running example images

<table>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/mobile_loggin.jpg" alt="Login screen" width="240" />
                <figcaption>Login screen (just for unauthenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_register.jpg" alt="Register screen" width="240" />
                <figcaption>Register screen (just for unauthenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_forgot.jpg" alt="Recover password screen" width="240" />
                <figcaption>Recover password screen (just for unauthenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/mobile_home.jpg" alt="Home screen" width="240" />
                <figcaption>Home screen with the total connections (just for authenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_list_filter.jpg" alt="TeacherList screen" width="240" />
                <figcaption>TeacherList screen with basic search form filter (just for authenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_list_filter_result.jpg" alt="TeacherList screen" width="240" />
                <figcaption>TeacherList screen with search results (just for authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
    <tr>
        <td>
            <figure>
                <img src=".gitimages/mobile_favorites.jpg" alt="Favorite screen" width="240" />
                <figcaption>Favorite screen with favorited classes (just for authenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_giveclasses.jpg" alt="GiveClasses screen" width="240" />
                <figcaption>GiveClasses screen with advice to use website to create a class (just for authenticated users)</figcaption>
            </figure>
        </td>
        <td>
            <figure>
                <img src=".gitimages/mobile_profile.jpg" alt="Profile screen" width="240" />
                <figcaption>Profile screen (just for authenticated users)</figcaption>
            </figure>
        </td>
    </tr>
</table>